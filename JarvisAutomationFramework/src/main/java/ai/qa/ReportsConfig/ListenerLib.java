package ai.qa.ReportsConfig;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.Status;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.IConstants;

public class ListenerLib implements ITestListener{

	
	public void onTestStart(ITestResult result) {
		ExtentManager.testlog.log(Status.INFO, result.getName()+" is Started");
		
	}
	
	public void onTestSuccess(ITestResult result) {
		ExtentManager.testlog.log(Status.PASS, result.getName()+" is Pass");
		
	}
	
	public void onTestFailure(ITestResult result) {
		ExtentManager.testlog.log(Status.FAIL, result.getName()+" is Fail");
		File source = ((TakesScreenshot)BaseClass.driver).getScreenshotAs(OutputType.FILE);
		String path = IConstants.userDirectoryPath+"/ScreenShots/"+ result.getName() + ".png";
		File destination = new File(path);
		
		try {
			FileUtils.copyFile(source, destination);
			ExtentManager.testlog.addScreenCaptureFromPath(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	public void onTestSkipped(ITestResult result) {
		ExtentManager.testlog.log(Status.SKIP, result.getName()+" is fail");
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}



	public void onStart(ITestContext context) {
		ExtentManager.report(context);
		ExtentManager.testlog.log(Status.INFO, context.getName()+"suite is started");
		
	}

	public void onFinish(ITestContext context) {
		ExtentManager.testlog.log(Status.INFO, context.getName()+" Suite is Completed");
		ExtentManager.report.flush();
		
	}

	
}
