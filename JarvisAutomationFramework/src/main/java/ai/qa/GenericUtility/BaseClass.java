package ai.qa.GenericUtility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.ExecutionsPage;
import ai.qa.Pages.LoginPage;

public class BaseClass {
	
	public static WebDriver driver;
	FileUtility flib = new FileUtility();
	WebDriverUtility wlib = new WebDriverUtility();
	SimpleDateFormat df=new SimpleDateFormat("dd-MM-yy-hh.mm.ss");
	Date date=new Date();
	String time = df.format(date);
	
	/*@BeforeSuite
	public void configBeforeSuite()
	{
		
	}*/
	
	/*@BeforeTest
	public void configBeforeTest() throws Throwable
	{
		
	}*/
	
	/**
	 * Used to launch the browser and enter the URL
	 * @throws Throwable
	 */
	@BeforeClass
	public void configBeforeClass() throws Throwable
	{
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		String URL = flib.getPropertyKeyValue("url");
		driver.get(URL);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	/**
	 * Used to login to the application
	 */
	@BeforeMethod
	public void configBeforeMethod() throws Throwable
	{
		LoginPage lpage = new LoginPage(driver);
		lpage.loginToApp(flib.getPropertyKeyValue("username"), flib.getPropertyKeyValue("password"));
		
	}
	
	@AfterMethod
	public void configAfterMethod()
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		aPage.logout();
		
	}
	
	@AfterClass
	public void configAfterClass()
	{
		driver.close();
	}
	/*
	@AfterTest
	public void configAfterTest()
	{
		
	}*/
	
	@AfterSuite
	public void configAfterSuite() throws EmailException
	{
	
		EmailAttachment attachment = new EmailAttachment();
		  attachment.setPath("C:\\Users\\HP\\git\\Jarvis\\JarvisAutomationFramework\\Reports\\ApplicationBuild_"+time+".html");
		  attachment.setDisposition(EmailAttachment.ATTACHMENT);

		  // Create the email message
		  MultiPartEmail email = new MultiPartEmail();
		  email.setHostName("smtp.gmail.com");
		  email.setSmtpPort(465);
		  email.setAuthenticator(new DefaultAuthenticator("mounika.peddineni@gmail.com", "Sujatha@33"));
		  email.setSSLOnConnect(true);
		  //email.setStartTLSEnabled(true);
		  email.addTo("kavya.pm@jiffy.ai", "Kavya P M");
		  email.addTo("subin.perumbidy@jiffy.ai", "Subin");
		  email.setFrom("mounika.p@jiffy.ai", "Mounika");
		  email.setSubject("Daily Automation Status Report");
		  email.setMsg("Hi Team"+"\n"+"\n"+"Here is today's report"+"\n"+"\n"+"\n"+"\n"+"Note: Please download the attachment and open with chrome");

		  // add the attachment
		  email.attach(attachment);

		  // send the email
		  email.send();
	}
	
}
