package ai.qa.GenericUtility;
/**
 * 
 * @author Mounika
 *
 */
public interface IConstants {
	
	public String userDirectoryPath = System.getProperty("user.dir");
	public String commonDataFilePath = userDirectoryPath+"/src/test/resources/commonConfig/commonData.Properties";
	public String excelFilePath = userDirectoryPath+"/src/test/resources/testData/TestData.xlsx";
	public String fileInputImagePath = userDirectoryPath+"/src/test/resources/testData/InputField.PNG";
	public String openButtonImagePath = userDirectoryPath+"/src/test/resources/testData/OpenButton.PNG";
}
