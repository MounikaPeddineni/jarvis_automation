package ai.qa.GenericUtility;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class WebDriverUtility {
		
		
		/**
		 *  used to wait for expected element visibility in GUI
		 * @param driver
		 * @param element
		 */
		public void waitForElementtVisibality(WebDriver driver, WebElement element) {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(element));
		}	
		
		/**
		 * Used to wait until the element is invisible
		 * @param driver
		 * @param element
		 */
		public void waitForElementtInvisibality(WebDriver driver, WebElement element) {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.invisibilityOf(element));
		}
		/**
		 * used to wait & click for expected element in GUI
		 * @param element
		 * @throws Throwable
		 */
		public void waitAndClick( WebElement element) throws Throwable {
			  int count = 0;
		        while(count < 40) {
				   try {
					   element.click();
				     break;
				   }catch (Throwable e) {
					count++;
				   }
		        }
		}
		
		/**
		 * used to wait for expected element in GUI
		 * @param element
		 * @throws Throwable
		 */
		public void waitforElement( WebElement element) throws Throwable {
			  int count = 0;
		        while(count < 40) {
				   try {
					   element.isDisplayed();
				     break;
				   }catch (Throwable e) {
					count++;
				   }
		        }
		}
		
		/**
		 * select the value from the dropDown based on Visible text
		 * @param element
		 * @param options
		 */
		public void select(WebElement element, String options) {
			Select sel = new Select(element);
			sel.selectByVisibleText(options);
		}
		
		
		/**
		 * select the value from the dropDown based on index
		 * @param element
		 * @param options
		 */
		public void select(WebElement element, int index) {
			Select sel = new Select(element);
			sel.selectByIndex(index);
		}
		
		/**
		 *  Used to switch to another browser window based on browser partial / complete title
		 * @param driver
		 * @param browserTitle
		 */
		public void switchToBrowser(WebDriver driver,String browserTitle) {
			 Set<String> set = driver.getWindowHandles();
			  
			  for(String act : set) {
				  driver.switchTo().window(act);
				  String actPageTile = driver.getTitle();
				  if(actPageTile.contains(browserTitle)) {
					  break;
				  }
			  }
		}
		
		/**
		 * Used to Switch to Alert Popup & click on OK button
		 * @param driver
		 */
		public void alerttOK(WebDriver driver) {
			
			driver.switchTo().alert().accept();
		}
		
		/**
		 * Used to Switch to Alert Popup & click on cancel button
		 * @param driver
		 */
	   public void alerttCancel(WebDriver driver) {
			
			driver.switchTo().alert().dismiss();
		}
		
		/**
		 * Used to take mouse cursor on expected element on the browser
		 * @param driver
		 * @param element
		 */
		public void moveToExpectedElement(WebDriver driver , WebElement element) {
			Actions act = new Actions(driver);
			act.moveToElement(element).perform();
		}
		
		/**
		 * Used to upload a file
		 * @param element
		 * @param fileInputImagePath
		 * @param openButtonImagePath
		 * @param filePath
		 * @throws Throwable
		 */
		public void handleFileUpload(WebElement element, String fileInputImagePath, String openButtonImagePath, String filePath) throws Throwable
		{
			Pattern fileInput = new Pattern(fileInputImagePath);
			Pattern openButton = new Pattern(openButtonImagePath);
			element.click();
			Screen s = new Screen();
			s.wait(fileInput,20);
			s.type(filePath);
			s.click(openButton);
			
		}
		/**
		 * Used to capture screenshot 
		 * @param testName
		 * @param driver
		 * @return
		 * @throws IOException
		 */
		public void capture(String testName, WebDriver driver) throws IOException {
			File source = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			String path = "./ScreenShots/" + testName + ".png";
			File destination = new File(path);
			FileUtils.copyFile(source, destination);
		}
		
	
}
