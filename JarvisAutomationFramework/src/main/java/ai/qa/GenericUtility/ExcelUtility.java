package ai.qa.GenericUtility;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * 
 * @author Mounika
 *
 */
public class ExcelUtility {
	
	/**
	 * Used to get data from excel sheet using sheetName, rowNum, colNum
	 * @param sheetName
	 * @param rowNum
	 * @param colNum
	 * @return value
	 * @throws Throwable
	 */
	public String getExcelData(String sheetName , int rowNum , int colNum) throws Throwable {
		FileInputStream fis = new FileInputStream(IConstants.excelFilePath);
		Workbook wb = WorkbookFactory.create(fis);
		String value = wb.getSheet(sheetName).getRow(rowNum).getCell(colNum).getStringCellValue();
		wb.close();
		return value;				
	}
	
	/**
	 * Used to write the data in to excel
	 * @param sheetName
	 * @param rowNum
	 * @param colNum
	 * @param data
	 * @throws Throwable
	 */
	public void setExcelData(String sheetName, int rowNum, int colNum, String data) throws Throwable
	{
		FileInputStream fis = new FileInputStream(IConstants.excelFilePath);
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet(sheetName);
		Row r = sh.getRow(rowNum);
		Cell c = r.createCell(colNum);
		c.setCellValue(data);
		FileOutputStream fos = new FileOutputStream(IConstants.excelFilePath);
		wb.write(fos);
		wb.close();
		
	}
	
	/**
	 * Used to create new sheet in the excel
	 * @param sheetName
	 * @throws Throwable
	 */
	public void createExcelSheet(String sheetName) throws Throwable
	{
		FileInputStream fis = new FileInputStream(IConstants.excelFilePath);
		Workbook wb = WorkbookFactory.create(fis);
		FileOutputStream fos = new FileOutputStream(IConstants.excelFilePath);
		wb.createSheet(sheetName);
		wb.write(fos);
		wb.close();
	}
	
	
}
