package ai.qa.GenericUtility;

import java.util.Date;
import java.util.Random;
/**
 * 
 * @author Mounika
 *
 */
public class JavaUtility {

	/**
	 * Used to generate the random number
	 * @return
	 */
	public  String getRanDomData() {
		Random random = new Random();
		int ranInt = random.nextInt(1000);
		return ""+ranInt;
	}
	
	/**
	 * Used to generate the current system date
	 * @return 
	 * @return
	 */
	public  String getCurrentSystemDate() {
        Date date = new Date();
        String currentDate = date.toString();
        return currentDate;
	}
	
	/**
	 * Used to get current time
	 * @return
	 */
	public  String getCurrentSystemTime() {
        Date date = new Date();
        String currentDate = date.toString();
        String arr[] = currentDate.split(" ");
        return arr[3];
	}
	
}
