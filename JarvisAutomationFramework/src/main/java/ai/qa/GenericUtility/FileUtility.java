package ai.qa.GenericUtility;

import java.io.FileInputStream;
import java.util.Properties;
/**
 * 
 * @author Mounika
 *
 */

public class FileUtility {

	/**
	 * Used to get data from property file using key
	 * @param key
	 * @return value
	 * @throws Throwable
	 */
	public String getPropertyKeyValue(String key) throws Throwable  {
		  
		FileInputStream fis = new FileInputStream(IConstants.commonDataFilePath);
		 Properties pObj = new Properties();
		 pObj.load(fis);
		 String value = pObj.getProperty(key);
		 return value;
	
	}

}
