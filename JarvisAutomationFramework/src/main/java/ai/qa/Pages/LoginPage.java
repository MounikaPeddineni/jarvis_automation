package ai.qa.Pages;

/**
 * @author Mounika
 */
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class LoginPage {

	WebDriver driver;
	
	@FindBy(id="username")
	private WebElement userName;

	@FindBy(id="password")
	private WebElement passWord;
	
	@FindBy(xpath="//span[.='LOG IN']")
	private WebElement loginButton;
	
	@FindBy(id="forgotPassword")
	private WebElement forgotPassword;
	
	@FindBy(xpath="//span[.='Try logging in']")
	private WebElement tryLogginglink;
	
	public LoginPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
	public WebElement getUserName() {
		return userName;
	}

	public WebElement getPassWord() {
		return passWord;
	}

	public WebElement getLoginButton() {
		return loginButton;
	}
	
	public WebElement getForgotPassword() {
		return forgotPassword;
	}
	
	public WebElement gettryLogginglink() {
		return tryLogginglink;
	}

	/**
	 * Used to login to the application using userName and passWord
	 * @throws Throwable 
	 */
	public void loginToApp(String un, String pwd) throws Throwable
	{
		userName.sendKeys(un);
		passWord.sendKeys(pwd);
		loginButton.click();
	}
	
	/**
	 * Used to reset password using email id
	 * @param emailId
	 */
	public void resetPassword(String emailId)
	{
		forgotPassword.click();
		driver.findElement(By.id("emailId")).sendKeys(emailId);
		//driver.findElement(By.xpath("//button[.='SEND']")).click();
	}
	
	/**
	 * Used to verify Try Logging In link is redirecting to login page
	 * 
	 */
	public void tryLogging()
	{
		String expTitle = driver.getTitle();
		forgotPassword.click();
		tryLogginglink.click();
		String actTitle = driver.getTitle();
		Assert.assertEquals(actTitle, expTitle);
		
	}
	

	
	
	
}
