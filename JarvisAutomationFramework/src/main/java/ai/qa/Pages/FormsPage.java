package ai.qa.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormsPage {
	
	WebDriver driver;
	
	@FindBy(xpath="//button[.='ADD NEW FORM']")
	private WebElement addNewFormButton;
	
	@FindBy(xpath="//a[.='General Settings']")
	private WebElement generalSettingsTab;
	
	@FindBy(xpath="//a[.='Button Settings']")
	private WebElement buttonSettingsTab;
	
	@FindBy(xpath="//a[.='Column Settings']")
	private WebElement columnSettingsTab;
	
	@FindBy(xpath = "//div[@class='select__value-container select__value-container--has-value css-1hwfws3']")
	private WebElement selectTableDropdown;
	
	@FindBy(css="i[class='_docubefonts__icons___1vjGR _docubefonts__icon-duplicate___aOLdl']")
	private WebElement duplicateFormIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-cross___3qQhl']")
	private WebElement deleteFormIcon;
	
	@FindBy(id="textfield-FormName")
	private WebElement formNameTextField;
	
	@FindBy(id="selectRoles_0")
	private WebElement selectRolesDropdown;
	
	@FindBy(id="selectStates_0")
	private WebElement selectStatesDropdown;
	
	@FindBy(css="i[class='_docubefonts__icons___1vjGR _docubefonts__icon-add___1s2Pi forms__editor__buttonIcon___TikSP']")
	private WebElement addIcon;
	
	@FindBy(xpath="//button[text()='CANCEL']")
	private WebElement cancelButton;
	
	@FindBy(xpath="//button[text()='SAVE']")
	private WebElement saveButton;
	
	public FormsPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public WebElement getAddNewFormButton() {
		return addNewFormButton;
	}

	public WebElement getGeneralSettingsTab() {
		return generalSettingsTab;
	}

	public WebElement getButtonSettingsTab() {
		return buttonSettingsTab;
	}

	public WebElement getColumnSettingsTab() {
		return columnSettingsTab;
	}

	public WebElement getSelectTableDropdown() {
		return selectTableDropdown;
	}

	public WebElement getDuplicateFormIcon() {
		return duplicateFormIcon;
	}

	public WebElement getDeleteFormIcon() {
		return deleteFormIcon;
	}
	
	public WebElement getFormNameTextField() {
		return formNameTextField;
	}

	public WebElement getSelectRolesDropdown() {
		return selectRolesDropdown;
	}

	public WebElement getSelectStatesDropdown() {
		return selectStatesDropdown;
	}

	public WebElement getAddIcon() {
		return addIcon;
	}

	public WebElement getCancelButton() {
		return cancelButton;
	}

	public WebElement getSaveButton() {
		return saveButton;
	}

	public void navigateToForms()
	{
		AppPage appPage = new AppPage(driver);
		appPage.getFormsIcon().click();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
