package ai.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import ai.qa.GenericUtility.ExcelUtility;

/**
 * @author Mounika
 */
public class ExecutionsPage {

	WebDriver driver; 
	ExcelUtility elib = new ExcelUtility();
	
	@FindBy(xpath="//i[@class='icomoon arrow-left2']")
	private WebElement backIcon;
	
	@FindBy(xpath="//i[@class='icomoon list1']")
	private WebElement executionHistoryIcon;
	
	@FindBy(xpath="//i[@class='icomoon list5']")
	private WebElement menuIcon;
	
	@FindBy(xpath="//i[@class='icomoon icon-play1']")
	private WebElement runTasksIcon;
	
	@FindBy(xpath="//i[@class='icomoon icon-clock2']")
	private WebElement scheduledRunsIcon;
	
	@FindBy(id="jiffy-table-pagination-right-icon")
	private WebElement nextPageIcon;
	
	@FindBy(xpath="//button[@class='dropbtn']")
	private WebElement profileBtn;
	
	@FindBy(xpath="//span[.='Logout']")
	private WebElement logoutBtn;
	
	
	public WebElement getBackIcon() {
		return backIcon;
	}

	public WebElement getExecutionHistoryIcon() {
		return executionHistoryIcon;
	}

	public WebElement getMenuIcon() {
		return menuIcon;
	}

	public WebElement getRunTasksIcon() {
		return runTasksIcon;
	}

	public WebElement getScheduledRunsIcon() {
		return scheduledRunsIcon;
	}

	public WebElement getnextPageIcon() {
		return nextPageIcon;
	}
	
	public WebElement getProfileBtn() {
		return profileBtn;
	}
	
	public WebElement getLogoutBtn() {
		return logoutBtn;
	}
	
	public ExecutionsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Used to run the task in execution bot based on taskname
	 * @param taskName
	 */
	public void runTaskInExecutionBot(String taskName)
	{
		TaskPage tPage = new TaskPage(driver);
		tPage.getTasksMenuIcon().click();
		tPage.getHistoryIcon().click();
		for (String handle: driver.getWindowHandles()) {
			   driver.switchTo().window(handle);
		}
		int count = 0;
		while(count<15)
		{
			try
				{
				driver.findElement(By.xpath("//table/tbody/tr/td[.='"+taskName+"']/preceding::span[3]")).click();
				break;
				}
			catch(Throwable e)
				{
					driver.findElement(By.xpath("//a[.='Next']")).click();
					count++;
				}
			
		}
		logoutinExecutionPage();
		driver.close();
		for (String handle: driver.getWindowHandles()) {
			   driver.switchTo().window(handle);
		}

	}
	
	/**
	 * Used to logout in execution page
	 */
	public void logoutinExecutionPage()
	{
		profileBtn.click();
		logoutBtn.click();
	}
	
	/**
	 * Used to check execution status of a task 
	 * @param taskName
	 * @throws Throwable
	 */
	public void checkStatus(String taskName) throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExecutionsPage ePage = new ExecutionsPage(driver);
		TaskPage tPage = new TaskPage(driver);
		ExcelUtility elib = new ExcelUtility();
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		tPage.getTasksMenuIcon().click();
		tPage.getHistoryIcon().click();
		for (String handle: driver.getWindowHandles()) {
			   driver.switchTo().window(handle);
		}
		ePage.getExecutionHistoryIcon().click();
		int count = 0;
		String status = "";
		while(count<10) {
					try {
						
						if(driver.findElement(By.xpath("//span[.='"+taskName+"']")).isDisplayed()) 
						{
						  status = driver.findElement(By.xpath("//span[.='"+taskName+"']/../../div[5]")).getText();	
						}
						break;
					}
					catch(Throwable e)
					{
					ePage.getnextPageIcon().click();
					count++;
					}
		}
		
			if(status.equals("PASSED"))
				Assert.assertTrue(status.equals("PASSED"));
			else if(status.equals("FAILED"))
				Assert.fail();
			else if(status.equals("INVALID"))
				Assert.fail("Invalid Status");	
		
			logoutinExecutionPage();
			driver.close();
			for (String handle: driver.getWindowHandles()) {
				   driver.switchTo().window(handle);
			}
		
	}
}
