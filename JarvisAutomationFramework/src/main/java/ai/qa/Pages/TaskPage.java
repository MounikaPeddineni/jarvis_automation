package ai.qa.Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
/**
 * @author Mounika
 */
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.WebDriverUtility;

public class TaskPage extends BaseClass{

	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-menu-tasks___RDi7Y']")
	private WebElement tasksMenuIcon;
	
	@FindBy(xpath="//button[.='CREATE']")
	private WebElement createButton;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-add___1s2Pi']")
	private WebElement createIcon;
	
	@FindBy(id="textfield-TaskName")
	private WebElement taskNameField;
	
	@FindBy(xpath="//button[.='CANCEL']")
	private WebElement cancelButton;
	
	@FindBy(xpath="//button[.='Create']")
	private WebElement createTaskButton;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-history___1p9R5']")
	private WebElement historyIcon;
	
	public TaskPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getTasksMenuIcon() {
		return tasksMenuIcon;
	}

	public WebElement getCreateButton() {
		return createButton;
	}
	
	public WebElement getcreateIcon() {
		return createIcon;
	}
	
	public WebElement getTaskName() {
		return taskNameField;
	}

	public WebElement getCancelButton() {
		return cancelButton;
	}

	public WebElement getCreateTaskButton() {
		return createTaskButton;
	}
	
	public WebElement getHistoryIcon() {
		return historyIcon;
	}
	
	public void navigateToTasks()
	{
		AppPage appPage = new AppPage(driver);
		appPage.getTasksIcon().click();
	}

	/**
	 * Used to create task
	 * @param title
	 * @throws Throwable 
	 */
	public void createTask(String title) throws Throwable
	{
		WebDriverUtility wlib = new WebDriverUtility();
		tasksMenuIcon.click();
		try
		{
			createButton.click();
		}
		catch(Throwable e)
		{
			createIcon.click();
		}
		
		taskNameField.sendKeys(title);
		createTaskButton.click();
		Thread.sleep(500);
		for (String handle: driver.getWindowHandles()) {
			   driver.switchTo().window(handle);		   
			   
		}
		WebElement element = driver.findElement(By.xpath("//button[.='EDIT']"));
		wlib.waitAndClick(element);
		Assert.assertTrue(driver.getTitle().contains(title));
		driver.close();
		
		for (String handle: driver.getWindowHandles()) {
			   driver.switchTo().window(handle);		   
			   
		}
	}
	
	/**
	 * Used to open task based on taskname
	 * @param taskName
	 */
	public void openTask(String taskName)
	{
		tasksMenuIcon.click();
		int count = 0;
		while(count<15)
		{
			try
				{
				driver.findElement(By.xpath("//div[.='"+taskName+"']/preceding::a[1]")).click();
				break;
				}
			catch(Throwable e)
				{
					driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-caret-right___Ahz32']")).click();
					count++;
				}
			
		}
		
		for (String handle: driver.getWindowHandles()) {
			   driver.switchTo().window(handle);
		}
		Assert.assertTrue(driver.getTitle().contains(taskName));
	}
	
	
	/**
	 * Used to run the task in developer bot based on taskname
	 * @param taskName
	 * @throws Throwable
	 */	
	public void runTaskInDeveloperBot(String taskName) throws Throwable
		{
			tasksMenuIcon.click();			
			int count = 0;
			while(count<15)
			{
				try
					{
					openTask(taskName);
					break;
					}
				catch(Throwable e)
					{
						driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-caret-right___Ahz32']")).click();
						count++;
					}
				
			}
			for (String handle: driver.getWindowHandles()) {
				   driver.switchTo().window(handle);
			}
			driver.findElement(By.xpath("//button[.='EDIT']")).click();
			WebElement runButton = driver.findElement(By.xpath("//i[@class='icon-run iconmoon']"));
			runButton.click();	
	}
	

	public List<WebElement> getAllTasks() {
			
			navigateToTasks();
			List<WebElement> tasksList = new ArrayList<WebElement>();
			int count = 0;
			while(count<5)
			{		
				List<WebElement> List = driver.findElements(By.xpath("//div[@class='tablecomponent__contractcell___3wzdw']"));
				tasksList.addAll(List);
				if(driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-caret-right___Ahz32']")).isEnabled())
				{
					try {
					driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-caret-right___Ahz32']")).click();
					}
					catch(Throwable e)
					{
						break;
					}
					count++;
				}
				else
				{
					
				}
				
			}
			return tasksList;
		}
}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	