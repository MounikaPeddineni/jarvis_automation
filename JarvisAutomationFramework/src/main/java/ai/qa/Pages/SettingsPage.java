package ai.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class SettingsPage {

	WebDriver driver;
	
	public SettingsPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//span[@class='StyledIcon-sc-17a1525-0 jFamZw']")
	private WebElement createCategoryIcon;
	
	@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
	private WebElement CategoryNameTextField;
	
	@FindBy(xpath="//button[.='CANCEL']")
	private WebElement cancelButton;
	
	@FindBy(xpath="//button[.='SAVE']")
	private WebElement saveButton;

	public WebElement getCreateCategoryIcon() {
		return createCategoryIcon;
	}

	public WebElement getCategoryNameTextField() {
		return CategoryNameTextField;
	}

	public WebElement getCancelButton() {
		return cancelButton;
	}

	public WebElement getSaveButton() {
		return saveButton;
	}
	
	public void navigateToSettingsPage()
	{
		
	}
	public void createCategory(String categoryName)
	{
		createCategoryIcon.click();
		CategoryNameTextField.sendKeys(categoryName);
		saveButton.click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[@class='MuiAlert-message']")).isDisplayed());
	}
	
	public void deleteCategory(String categoryName)
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		aPage.getSettingsIcon().click();
		driver.findElement(By.xpath("//div[.='Automation']//span[@class='StyledIcon-sc-17a1525-0 kgsaEb']")).click();
		driver.findElement(By.xpath("//button[.='DELETE']")).click();
	}
}
