package ai.qa.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AppPage {

	WebDriver driver;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-view-list___3-Rxv']")
	private WebElement menuIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-menu-presentation___2uVRA']")
	private WebElement presentationsIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-menu-tasks___RDi7Y']")
	private WebElement tasksIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-menu-datasets___3JcLB']")
	private WebElement datasetsIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-forms___jkvBS']")
	private WebElement formsIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-menu-models___3yB1h']")
	private WebElement modelsIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-view-more-bottom___251Co']")
	private WebElement viewMoreIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-view-more-top___1nUTM']")
	private WebElement viewLessIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-menu-filesets___1LBnL']")
	private WebElement filesetsIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-menu-clusters___bGpIG']")
	private WebElement clustersIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-account-circle___dWsRP']")
	private WebElement usersIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-meu-vault___1j2iB']")
	private WebElement securevaultIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-table-properties___loOFJ']")
	private WebElement appsettingsIcon;

	public WebElement getMenuIcon() {
		return menuIcon;
	}

	public WebElement getPresentationsIcon() {
		return presentationsIcon;
	}

	public WebElement getTasksIcon() {
		return tasksIcon;
	}

	public WebElement getDatasetsIcon() {
		return datasetsIcon;
	}

	public WebElement getFormsIcon() {
		return formsIcon;
	}

	public WebElement getModelsIcon() {
		return modelsIcon;
	}

	public WebElement getViewMoreIcon() {
		return viewMoreIcon;
	}

	public WebElement getViewLessIcon() {
		return viewLessIcon;
	}

	public WebElement getFilesetsIcon() {
		return filesetsIcon;
	}

	public WebElement getClustersIcon() {
		return clustersIcon;
	}

	public WebElement getUsersIcon() {
		return usersIcon;
	}

	public WebElement getSecurevaultIcon() {
		return securevaultIcon;
	}

	public WebElement getAppsettingsIcon() {
		return appsettingsIcon;
	}
	
	public AppPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
