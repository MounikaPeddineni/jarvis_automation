package ai.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import ai.qa.GenericUtility.WebDriverUtility;

public class ClustersPage {

	WebDriver driver;
	WebDriverUtility wlib = new WebDriverUtility();
	
	@FindBy(xpath="//button[.='CREATE']")
	private WebElement createClusterButton;
	
	@FindBy(id="textfield-Name")
	private WebElement clusterNameTextField;
	
	@FindBy(id="textfield-Description")
	private WebElement clusterDescriptionTextField;
	
	@FindBy(xpath="//div[@class='select__value-container select__value-container--is-multi css-1hwfws3']")
	private WebElement selectMachineListBox;
	
	@FindBy(xpath="//button[.='SAVE']")
	private WebElement saveClusterButton;
	
	@FindBy(xpath="//button[.='CANCEL']")
	private WebElement cancelClusterButton;
	
	@FindBy(xpath="//button[@class='mdl-button mdl-js-button undefined mdl-button--icon icon-button__dcb-button--icon___Xv2Oe']")
	private WebElement createClusterIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-dropdown___3ajJu jarvis-select__jrvs-select__dropdown___3c18B']")
	private WebElement listArrow;

	@FindBy(xpath="//button[.='YES']")
	private WebElement yesButton;
	
	@FindBy(xpath="//button[.='NO']")
	private WebElement NoButton;
	
	public WebElement getCreateClustorButton() {
		return createClusterButton;
	}

	public WebElement getClusterNameTextField() {
		return clusterNameTextField;
	}

	public WebElement getClusterDescriptionTextField() {
		return clusterDescriptionTextField;
	}

	public WebElement getSelectMachineListBox() {
		return selectMachineListBox;
	}

	public WebElement getSaveClusterButton() {
		return saveClusterButton;
	}

	public WebElement getCancelClusterButton() {
		return cancelClusterButton;
	}
	
	public WebElement getcreateClusterIcon() {
		return createClusterIcon;
	}
	
	public WebElement getListArrow() {
		return listArrow;
	}

	public WebElement getYesButton() {
		return yesButton;
	}

	public WebElement getNoButton() {
		return NoButton;
	}
	
	public ClustersPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Used to create a cluster based on clusterName, clusterDescription, machineName
	 * @param clusterName
	 * @param clusterDescription
	 * @param machineName
	 * @throws Throwable
	 */
	public void createCluster(String clusterName, String clusterDescription, String machineName) throws Throwable
	{
		AppPage aPage = new AppPage(driver);
		aPage.getViewMoreIcon().click();
		aPage.getClustersIcon().click();
		
		try
		{
			createClusterButton.click();
		}
		catch(Throwable e)
		{
			createClusterIcon.click();
		}
		
		clusterNameTextField.sendKeys(clusterName);
		clusterDescriptionTextField.sendKeys(clusterDescription);
		selectMachineListBox.click();
		driver.findElement(By.xpath("//div[@class='select__menu-list select__menu-list--is-multi css-11unzgr']//div[.='"+machineName+"']")).click();
		listArrow.click();
		//saveClusterButton.click();
		
	}
	
	/**
	 * Used to delete cluster
	 * @param clusterName
	 */
	public void deleteCluster(String clusterName)
	{
		AppPage aPage = new AppPage(driver);
		aPage.getViewMoreIcon().click();
		aPage.getClustersIcon().click();
		driver.findElement(By.xpath("//div[.='"+clusterName+"']/following::i[2]")).click();
		//yesButton.click();
	}
	
	
	
	
}
