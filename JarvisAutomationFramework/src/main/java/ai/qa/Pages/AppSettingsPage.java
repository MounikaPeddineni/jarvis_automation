package ai.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import ai.qa.GenericUtility.WebDriverUtility;

public class AppSettingsPage {
	
	WebDriver driver;
	WebDriverUtility wlib = new WebDriverUtility();
	
	@FindBy(xpath="//div[@class='AppSettings__dcb_AppSettings__leftComp--header___2ssM6']//button[@class='mdl-button mdl-js-button mdl-button--icon icon-button__dcb-button--icon___Xv2Oe']")
	private WebElement editIcon;
	
	@FindBy(id="textfield-AppName")
	private WebElement appNameTextField;
	
	@FindBy(id="textfield-AppDescription")
	private WebElement appDescriptionTextField;
	
	@FindBy(xpath="//div[@class='AppSettings__dcb_AppSettings__leftComp--imageComp___1_sN1']//div[@class='AppSettings__dcb_AppSettings__leftComp--upload___XpdXG']")
	private WebElement appImageEditIcon;
	
	@FindBy(xpath="//button[.='SAVE']")
	private WebElement saveButton;
	
	@FindBy(xpath="//button[.='CANCEL']")
	private WebElement cancelButton;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-add___1s2Pi']")
	private WebElement createUserRoleIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-arrow-forward___1tpH2']")
	private WebElement backArrowIcon;
	
	@FindBy(id="textfield-NewUserRole")
	private WebElement newUserRoleTextField;
	
	@FindBy(xpath="//div[@class='select__value-container select__value-container--is-multi css-1hwfws3']")
	private WebElement selectPresentationsListBox;
	
	@FindBy(xpath="//button[.='CREATE']")
	private WebElement createUserRoleButton;
	
	@FindBy(xpath="//div[@class='AppSettings__dcb_AppSettings__rightComp--Bottom--actions___CE-t-']//button[.='CANCEL']")
	private WebElement cancelUserRoleButton;
	
	public AppSettingsPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getEditIcon() {
		return editIcon;
	}

	public WebElement getAppNameTextField() {
		return appNameTextField;
	}

	public WebElement getAppDescriptionTextField() {
		return appDescriptionTextField;
	}

	public WebElement getAppImageEditIcon() {
		return appImageEditIcon;
	}

	public WebElement getSaveButton() {
		return saveButton;
	}

	public WebElement getCancelButton() {
		return cancelButton;
	}

	public WebElement getCreateUserRoleIcon() {
		return createUserRoleIcon;
	}

	public WebElement getBackArrowIcon() {
		return backArrowIcon;
	}

	public WebElement getNewUserRoleTextField() {
		return newUserRoleTextField;
	}

	public WebElement getSelectPresentationsListBox() {
		return selectPresentationsListBox;
	}

	public WebElement getCreateUserRoleButton() {
		return createUserRoleButton;
	}

	public WebElement getCancelUserRoleButton() {
		return cancelUserRoleButton;
	}
	
	/**
	 * Used to edit App Name
	 * @param appName
	 */
	public void editAppName(String appName)
	{
		AppPage aPage = new AppPage(driver);
		aPage.getViewMoreIcon().click();
		aPage.getAppsettingsIcon().click();
		editIcon.click();
		appNameTextField.clear();
		appNameTextField.sendKeys(appName);
		saveButton.click();
	}
	
	/**
	 * Used to edit App Description
	 * @param appDescription
	 */
	public void editAppDescription(String appDescription)
	{
		AppPage aPage = new AppPage(driver);
		aPage.getViewMoreIcon().click();
		aPage.getAppsettingsIcon().click();
		editIcon.click();
		appDescriptionTextField.clear();
		appDescriptionTextField.sendKeys(appDescription);
		saveButton.click();
	}
	
	/**
	 * Used to edit App Image
	 * @param fileInputImagePath
	 * @param openButtonImagePath
	 * @param filePath
	 * @throws Throwable
	 */
	public void editAppImage(String fileInputImagePath, String openButtonImagePath, String filePath) throws Throwable {
		AppPage aPage = new AppPage(driver);
		aPage.getViewMoreIcon().click();
		aPage.getAppsettingsIcon().click();
		editIcon.click();
		WebElement element = driver.findElement(By.xpath("//div[.='App Image']//div[@class='AppSettings__dcb_AppSettings__leftComp--upload___XpdXG']"));
		wlib.handleFileUpload(element, fileInputImagePath, openButtonImagePath, filePath);
		saveButton.click();
		
	}
	
	public void createRole(String roleName) throws Throwable {
		AppPage aPage = new AppPage(driver);
		aPage.getViewMoreIcon().click();
		aPage.getAppsettingsIcon().click();
		createUserRoleIcon.click();
		newUserRoleTextField.sendKeys(roleName);
		selectPresentationsListBox.click();
		driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		createUserRoleButton.click();
	}
	
}
