package ai.qa.Pages;

/**
 * @author Mounika
 */
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import ai.qa.GenericUtility.IConstants;
import ai.qa.GenericUtility.WebDriverUtility;

public class AppSummaryPage {
	
	WebDriver driver;
	WebDriverUtility wlib = new WebDriverUtility();
	
	@FindBy(xpath = "//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-caret-down___3phls']")
	private WebElement profileButton;
	
	@FindBy(xpath="//span[.='Logout']")
	private WebElement logoutButton;
	
	@FindBy(xpath="//span[.='View/Edit Profile']")
	private WebElement viewProfileButton;
	
	@FindBy(id="textfield-AppName")
	private WebElement appName;
	
	@FindBy(id="textfield-AppDescription")
	private WebElement appDescription;
	
	@FindBy(xpath="//div[.='Browse']")
	private WebElement browseButton;
	
	@FindBy(xpath="//button[.='Create']")
	private WebElement createAppButton;
	
	@FindBy(xpath="//button[.='CANCEL']")
	private WebElement cancelAppButton;
	
	@FindBy(xpath="//span[@class='StyledIcon-sc-17a1525-0 hOtJqN']")
	private WebElement appSummaryIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-menu-users___2qCTd']")
	private WebElement usersIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-robot___22RJ5']")
	private WebElement botManagementIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-table-properties___loOFJ']")
	private WebElement settingsIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-meu-vault___1j2iB']")
	private WebElement tenantVaultIcon;
	
	public AppSummaryPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getProfileButton() {
		return profileButton;
	}

	public WebElement getLogoutButton() {
		return logoutButton;
	}

	public WebElement getViewProfileButton() {
		return viewProfileButton;
	}

	public WebElement getAppName() {
		return appName;
	}

	public WebElement getAppDescription() {
		return appDescription;
	}

	public WebElement getBrowseButton() {
		return browseButton;
	}

	public WebElement getCreateAppButton() {
		return createAppButton;
	}

	public WebElement getCancelAppButton() {
		return cancelAppButton;
	}
	
	public WebElement getAppSummaryIcon() {
		return appSummaryIcon;
	}
	
	public WebElement getUsersIcon() {
		return usersIcon;
	}

	public WebElement getBotManagementIcon() {
		return botManagementIcon;
	}

	public WebElement getSettingsIcon() {
		return settingsIcon;
	}

	public WebElement getTenantVaultIcon() {
		return tenantVaultIcon;
	}

	/**
	 * Used to navigate to view profile page
	 */
	public void profileViewEdit()
	{
		profileButton.click();
		viewProfileButton.click();
	}
	
	/**
	 * Used to logout from the application
	 */
	public void logout()
	{
		profileButton.click();
		logoutButton.click();
		
	}
	
	/**
	 * Used to create an app based on category and app image
	 * @param category
	 * @param appname
	 * @param appdescription
	 * @param fileInputImagePath
	 * @param openButtonImagePath
	 * @param filePath
	 * @throws Throwable
	 */
	public void createAppWithAppImage(String category, String appname, String appdescription, String filePath) throws Throwable
	{
		WebElement element = driver.findElement(By.xpath("//span[.='"+category+"']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		driver.findElement(By.xpath("//div[@class='App__dcb_app__container___2nBH3']/div[1]//span[.='"+category+"']/../../div[2]//button[.='new']")).click();
		appName.sendKeys(appname);
		appDescription.sendKeys(appdescription);
		wlib.handleFileUpload(browseButton, IConstants.fileInputImagePath, IConstants.openButtonImagePath, filePath);
		createAppButton.click();
        
	}
	
	/**
	 * Used to create an app based on category without app image
	 * @param category
	 * @param appname
	 * @param appdescription
	 * @throws Throwable
	 */
	public void createApp(String category, String appname, String appdescription) throws Throwable
	{
		WebElement element = driver.findElement(By.xpath("//span[.='"+category+"']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		driver.findElement(By.xpath("//div[@class='App__dcb_app__container___2nBH3']/div[1]//span[.='"+category+"']/../../div[2]//button[.='new']")).click();
		appName.sendKeys(appname);
		appDescription.sendKeys(appdescription);
		createAppButton.click();
        
	}
			
	
	/**
	 * Used to open an app based on categoryName and appName
	 * @param category
	 * @param appName
	 * @throws Throwable
	 */
	public void openApp(String category, String appName) throws Throwable
	{ 
		WebElement element = driver.findElement(By.xpath("//span[.='"+category+"']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		
		int count=0;
		while(count<10)
			{
				try {
					
					driver.findElement(By.xpath("//span[.='"+category+"']/following::div[.='"+appName+"']")).click();
					break;
				}
			
				catch(Throwable e)
				{
					driver.findElement(By.xpath("//span[.='"+category+"']/following::div//button[@class='mdl-button mdl-js-button mdl-button--icon icon-button__dcb-button--icon___Xv2Oe App__dcb_app__slide--next___2awX1']")).click();
					Thread.sleep(500);
					count++;
				}
				
			}					
	 }
}
		

	
		
		
		
		
		
	
	
