package ai.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PresentationsPage {
	WebDriver driver;

	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-add___1s2Pi']")
	private WebElement createPresentationIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-control-point___2mz6-']")
	private WebElement addPresentationTypeIcon;
	
	@FindBy(id="placeholder-3mn63")
	private WebElement presentationTitleField;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-done___3y3sB']")
	private WebElement tickIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-save1___3C0QZ']")
	private WebElement publishIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-refresh___3dGYd']")
	private WebElement reloadIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-app-running___3Dg1R']")
	private WebElement replaceDatasetsIcon;
	
	@FindBy(id="papers_0-menu")
	private WebElement moreIconPresentation;
	
	@FindBy(xpath="//li[.='Add Description']")
	private WebElement addDescriptionMenuItem;
	
	@FindBy(xpath="//li[.='Add KPI Ribbon']")
	private WebElement adKPIRibbonMenuItem;
	
	@FindBy(xpath="//li[.='Insert Above']")
	private WebElement insertAboveMenuItem;
	
	@FindBy(xpath="//li[.='Insert Above']")
	private WebElement insertBelowMenuItem;
	
	@FindBy(id="placeholder-22fm9")
	private WebElement cardTitleField;
	
	@FindBy(xpath="//li[@class='button-list__dcb-btn-list__item___1Ar0F']")
	private WebElement moreIconCard;
	
	public WebElement getCreatePresentationIcon() {
		return createPresentationIcon;
	}

	public WebElement getAddPresentationTypeIcon() {
		return addPresentationTypeIcon;
	}

	public WebElement getPresentationTitleField() {
		return presentationTitleField;
	}

	public WebElement getTickIcon() {
		return tickIcon;
	}

	public WebElement getPublishIcon() {
		return publishIcon;
	}

	public WebElement getReloadIcon() {
		return reloadIcon;
	}

	public WebElement getReplaceDatasetsIcon() {
		return replaceDatasetsIcon;
	}

	public WebElement getMoreIconPresentation() {
		return moreIconPresentation;
	}

	public WebElement getAddDescriptionMenuItem() {
		return addDescriptionMenuItem;
	}

	public WebElement getAdKPIRibbonMenuItem() {
		return adKPIRibbonMenuItem;
	}

	public WebElement getInsertAboveMenuItem() {
		return insertAboveMenuItem;
	}

	public WebElement getInsertBelowMenuItem() {
		return insertBelowMenuItem;
	}

	public WebElement getCardTitleField() {
		return cardTitleField;
	}

	public WebElement getMoreIconCard() {
		return moreIconCard;
	}

	public PresentationsPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void navigateToPresentations()
	{
		AppPage appPage = new AppPage(driver);
		appPage.getPresentationsIcon().click();
	}
	
	public void createPresentation(String presentationTitle)
	{
		createPresentationIcon.click();
		WebElement element = driver.findElement(By.id("placeholder-20fkq"));
		element.click();
		element.sendKeys(presentationTitle);
		//presentationTitleField.sendKeys(presentationTitle);
		publishIcon.click();
		
	}
}
