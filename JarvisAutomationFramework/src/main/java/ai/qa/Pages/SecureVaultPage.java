package ai.qa.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SecureVaultPage {

	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-add___1s2Pi']")
	private WebElement addSecureDataIcon;
	
	@FindBy(xpath="//div[@class='react-switch-bg']")
	private WebElement cyberArkToggleButton;
	
	@FindBy(xpath="//span[.='specific people']/../div[@class='vault__dcb_vaultButton__outer___egjDq']")
	private WebElement specificPeopleRadioButton;
	
	@FindBy(xpath="//span[.='global audience']/../div[@class='vault__dcb_vaultButton__outer___egjDq']")
	private WebElement globalAudienceRadioButton;
	
	@FindBy(xpath="//button[@class='mdl-button mdl-js-button mdl-button--icon icon-button__dcb-button--icon___Xv2Oe']")
	private WebElement addUserListIcon;
	
	@FindBy(xpath="//div[@class='select__value-container select__value-container--has-value css-1hwfws3']")
	private WebElement selectUsersListBox;
	
	
	
	
}
