package demo;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.FileUtility;
import ai.qa.GenericUtility.IConstants;
import ai.qa.GenericUtility.JavaUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AddUserPage;
import ai.qa.Pages.AppPage;
import ai.qa.Pages.AppSettingsPage;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.ClustersPage;
import ai.qa.Pages.ExecutionsPage;
import ai.qa.Pages.LoginPage;
import ai.qa.Pages.PresentationsPage;
import ai.qa.Pages.TaskPage;
import ai.qa.Pages.UsersPage;
import ai.qa.Pages.ViewProfilePage;


public class Sample extends BaseClass{
	
	@Test
	public void demo() throws Throwable {

		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		//aPage.logout();
		//to navigate to view/edit profile page
		//aPage.profileViewEdit();
		
		//to create an app based category
		//aPage.createApp("Multitenancy app", "Demo", "appdescription", "C:\\Users\\HP\\Desktop\\1591809198971.jpg");
		
		//to open app based on category and app name
		//aPage.openApp("123", "Automation_Test");
		
		/*AddUserPage user = new AddUserPage(driver);
		//to create an user
		user.createUser("user1", "user", "1", "user1@gmail.com");
		*/
		
		ExcelUtility elib = new ExcelUtility();
		//to get the data from excel
		//System.out.println(elib.getExcelData("P1 Regression", 1, 0));
		
		
		//to set the data into excel
		//elib.setExcelData("P1 Regression", 94, 1, "test");
		//System.out.println(elib.getExcelData("P1 Regression", 94, 1));
		
		//to create a new excel sheet
		//elib.createExcelSheet("demo1");
	
		/*FileUtility flib = new FileUtility();
		//to get the data from properties file
		System.out.println(flib.getPropertyKeyValue("browser"));
		*/
		
		/*JavaUtility jlib = new JavaUtility();
		//to get the current system date
		System.out.println(jlib.getCurrentSystemDate());
		//to generate the random number
		System.out.println(jlib.getRanDomData());
		*/
		
		/*AppSettingsPage asPage = new AppSettingsPage(driver);
		//to edit the appname
		asPage.editAppName("Automation_Test");
		//to edit the app description
		asPage.editAppDescription("Automation_Test");
		//to edit the app image
		asPage.editAppImage("C:\\Users\\HP\\Desktop\\InputField.PNG", "C:\\Users\\HP\\Desktop\\OpenButton.PNG", "C:\\Users\\HP\\Desktop\\1591809198971.jpg");
		*/
		
		/*ClustersPage cPage = new ClustersPage(driver);
		cPage.createCluster("Mouni", "demo", "m177");
		cPage.deleteCluster("Mounika_Cluster");
		*/
		
		/*LoginPage lPage = new LoginPage(driver);
		//to reset the password
		lPage.resetPassword("mounika.p@jiffy.ai");
		//to click on try logging in
		lPage.tryLogging();
		*/
		
		/*TaskPage tPage = new TaskPage(driver);
		//to create a task
		tPage.createTask("test");
		//to open a specific task
		tPage.openTask("CHILD TASK");
		//to run a task in developer bot
		tPage.runTaskInDeveloperBot("CHILD TASK");
		//to run a task in execution bot
		tPage.runTaskInExecutionBot("CHILD TASK");
		*/
		
		/*UsersPage uPage = new UsersPage(driver);
		//to search an user by username
		uPage.searchByUsername("mounika");
		//to search an user by emailid
		uPage.searchByEmailId("mounika.p@jiffy.ai");
		*/
		
		/*ViewProfilePage vPage = new ViewProfilePage(driver);
		//to change password
		vPage.changePassword("1234");   
		*/
		
		//WebDriverUtility wlib = new WebDriverUtility();
		//wlib.capture("demo");
		//driver.get("https://tenant.jiffy4-1qa.jiffy.ai");
        TakesScreenshot scrShot =((TakesScreenshot)driver);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        String path = "./ScreenShots/demo.png";
        File DestFile=new File(path);
        FileUtils.copyFile(SrcFile, DestFile);
	}	
}
