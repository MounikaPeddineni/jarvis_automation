package ai.qa.FormsTests;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC020_21_Test extends BaseClass{

	/**
	 * Used to verify delete icon is displayed for every form
	 * @throws Throwable
	 */
	@Test
	public void verifyTC020() throws Throwable {
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 41, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 41, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 41, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 41, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 41, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='accordion__dcb-accordion__outline___GAQ77 accordion__dcb-accordion__outline--horizontal___2AEhN undefined']/div"));
		for(WebElement w : oldlist)
		{
			w.click();
			Assert.assertTrue(fPage.getDeleteFormIcon().isDisplayed());
		}
		
	}
	
	/**
	 * Used to verify if there is only 1 form, delete button is not displayed
	 * @return 
	 * @throws Throwable
	 */
	@Test
	public void verifyTC021() throws Throwable {
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 43, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 43, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 43, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 43, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 43, 2), fPage.getSelectTableDropdown().getText());
		try {
            fPage.getDeleteFormIcon().isDisplayed();
        } catch (NoSuchElementException e) {
            System.out.println("Delete Form icon is not displayed");
        }
		
		
	}
}
