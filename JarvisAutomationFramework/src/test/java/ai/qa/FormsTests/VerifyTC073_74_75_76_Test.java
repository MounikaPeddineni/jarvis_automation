package ai.qa.FormsTests;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC073_74_75_76_Test extends BaseClass{

	/**
	 * Delete Role and state
	 * Used to verify role and state combination can be deleted
	 * @throws Throwable
	 */
	@Test
	public void verifyTC073() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 119, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 119, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 119, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 119, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 119, 2), fPage.getSelectTableDropdown().getText());
		int oldRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		int oldStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		if(oldRolesCount==1)
		{
			fPage.getAddIcon().click();
			fPage.getSaveButton().click();
			wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		}
		oldRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		oldStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-close___1_Pix forms__editor__buttonIcon___TikSP']")));
		driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-close___1_Pix forms__editor__buttonIcon___TikSP']")).click();
		Thread.sleep(200);
		int newRolesCount = 0;
		int count = 0;
		while(count<10)
		{
			try {
				newRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
				
				if(newRolesCount<oldRolesCount)
				{
			
				}
				break;
			}
			catch(Throwable e) {
				count++;
			}
		}
		newRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		int newStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		Assert.assertEquals(oldRolesCount-1, newRolesCount);
		Assert.assertEquals(oldStatessCount-1, newStatessCount);
	}
	
	/**
	 * Delete Role and state
	 * Used to verify if there is only 1 role and state combo, delete button is not displayed
	 * @throws Throwable
	 */
	@Test
	public void verifyTC074() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 121, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 121, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 121, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 121, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 121, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		int oldRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		if(oldRolesCount==1)
		{
			fPage.getAddIcon().click();
			fPage.getSaveButton().click();
			wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		}
		try {
			driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-close___1_Pix forms__editor__buttonIcon___TikSP']")).isDisplayed();
		}
		catch (NoSuchElementException e) {
            System.out.println("Delete icon is not displayed");
        }	
	}
	
	
	/**
	 * Delete Role and state
	 * Used to verify changes are saved when clicked on save button
	 * @throws Throwable
	 */
	@Test
	public void verifyTC075() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 123, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 123, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 123, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 123, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 123, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		int oldRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		int oldStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		if(oldRolesCount==1)
		{
			fPage.getAddIcon().click();
			fPage.getSaveButton().click();
			wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		}
		oldRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		oldStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-close___1_Pix forms__editor__buttonIcon___TikSP']")));
		driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-close___1_Pix forms__editor__buttonIcon___TikSP']")).click();	
		int newRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		int newStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		Assert.assertEquals(oldRolesCount-1, newRolesCount);
		Assert.assertEquals(oldStatessCount-1, newStatessCount);
	}
	
	/**
	 * Delete Role and state
	 * Used to verify changes are discarded when clicked on cancel button
	 * @throws Throwable
	 */
	@Test
	public void verifyTC076() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 125, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 125, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 125, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 125, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 125, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		int oldRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		int oldStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		fPage.getAddIcon().click();
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-close___1_Pix forms__editor__buttonIcon___TikSP']")));
		driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-close___1_Pix forms__editor__buttonIcon___TikSP']")).click();
		fPage.getCancelButton().click();
		Thread.sleep(200);	
		int newRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		int newStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		Assert.assertEquals(oldRolesCount, newRolesCount);
		Assert.assertEquals(oldStatessCount, newStatessCount);
	}
	
}
