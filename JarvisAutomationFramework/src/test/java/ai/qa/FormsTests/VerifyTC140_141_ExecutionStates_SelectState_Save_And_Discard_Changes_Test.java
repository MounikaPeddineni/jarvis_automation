package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC140_141_ExecutionStates_SelectState_Save_And_Discard_Changes_Test extends BaseClass{

	/**
	 * Execution States dropdown
	 * Used to verify changes are saved when clicked on save button
	 * @throws Throwable
	 */
	@Test
	public void verifyTC140_SelectState_Save_Button_Save_Changes() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 227, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 227, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 227, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 227, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 227, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		driver.findElement(By.xpath("//input[@value='CANCEL']//following::span[3]//div[@class='select__indicators css-1wy0on6']")).click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 227, 3));
		wlib.waitforElement(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 227, 3)+"']")));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		String statename = driver.findElement(By.xpath("//input[@value='CANCEL']//following::span[3]//div[@class='select__single-value css-1uccc91-singleValue']")).getText();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 227, 3), statename);
	}
	
	
	/**
	 * Execution States dropdown
	 * Used to verify changes are discarded when clicked on cancel button
	 * @throws Throwable
	 */
	/*@Test
	public void verifyTC141_SelectState_Cancel_Button_Discard_Changes() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 229, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 229, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 229, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 229, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 229, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		driver.findElement(By.xpath("//input[@value='CANCEL']//following::span[3]//div[@class='select__indicators css-1wy0on6']")).click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 229, 3));
		wlib.waitforElement(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 227, 3)+"']")));
		//Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		Thread.sleep(1000);
		fPage.getCancelButton().click();
		//Thread.sleep(1000);
		//wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		String taskname = driver.findElement(By.xpath("//input[@value='CANCEL']//following::span[3]//div[@class='select__single-value css-1uccc91-singleValue']")).getText();
		Assert.assertEquals(taskname, "Select");
	}*/
}
