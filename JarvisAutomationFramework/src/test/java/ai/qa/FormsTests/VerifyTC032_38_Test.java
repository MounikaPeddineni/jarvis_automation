package ai.qa.FormsTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.JavaUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC032_38_Test extends BaseClass{

	/**
	 * Rename Form
	 * Used to verify that the user can rename the form
	 * @throws Throwable
	 */
	@Test
	public void verifyTC032() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		JavaUtility jlib = new JavaUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 63, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 63, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 63, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 63, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 63, 2), fPage.getSelectTableDropdown().getText());
		for(int i=0; i<3; i++)
		{
			fPage.getFormNameTextField().clear();
		}
		String formName = elib.getExcelData("P1 Forms", 63, 2)+" "+jlib.getRanDomData();
		fPage.getFormNameTextField().sendKeys(formName);
		try {
			Assert.assertEquals(fPage.getFormNameTextField().getAttribute("value"), formName);
		}
		catch(Throwable e)
		{
			
		}
		
	}
	
	/**
	 * Rename Form
	 * Used to verify form name cannot be set as blank
	 */
	/*@Test
	public void verifyTC038() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 38, 2));
		driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		for(int i=0; i<3; i++)
		{
			fPage.getFormNameTextField().clear();
		}
		fPage.getSaveButton().click();
		
	}*/
}