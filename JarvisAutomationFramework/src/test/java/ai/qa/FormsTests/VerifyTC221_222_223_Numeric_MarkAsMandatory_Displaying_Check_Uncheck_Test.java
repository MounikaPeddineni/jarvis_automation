package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC221_222_223_Numeric_MarkAsMandatory_Displaying_Check_Uncheck_Test extends BaseClass{

	/**
	 * Numeric
	 * Verify Mark as mandatory option is displayed
	 * @throws Throwable
	 */
	@Test
	public void VerifyTC221_Numeric_MarkAsMandatory_Displaying() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 293, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 293, 2));
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 293, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 293, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 293, 2), fPage.getSelectTableDropdown().getText());
		fPage.getColumnSettingsTab().click();
		List<WebElement> colsList = driver.findElements(By.xpath("//div[@class='forms__columnRestrictions__columnItem___2IY09']"));
		for(WebElement w : colsList)
		{
			String text = w.getText();
		if(text.equals(elib.getExcelData("P1 Forms", 293, 3))) {
				
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")));
					driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
					try {
						driver.findElement(By.xpath("//div[.='Mark as mandatory']")).isDisplayed();
					}
					catch(Throwable e)
					{
						
					}
					driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
				}
			}
 
		}

	}
	
	/**
	 * Numeric
	 * Verify mandatory option can be checked
	 * @throws Throwable
	 */
	@Test
	public void VerifyTC222_Numeric_MarkAsMandatory_CanBe_Checked() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 295, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 295, 2));
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 295, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 295, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 295, 2), fPage.getSelectTableDropdown().getText());
		fPage.getColumnSettingsTab().click();
		List<WebElement> colsList = driver.findElements(By.xpath("//div[@class='forms__columnRestrictions__columnItem___2IY09']"));
		for(WebElement w : colsList)
		{
			String text = w.getText();
				if(text.equals(elib.getExcelData("P1 Forms", 295, 3))) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")));
					driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
					wlib.waitforElement(driver.findElement(By.xpath("//span[.='Mark as mandatory']")));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//span[.='Mark as mandatory']")));
					List<WebElement> list1 = driver.findElements(By.xpath("//span[@class='mdl-checkbox__label']"));
					for(WebElement element : list1)
					{
					if(element.getText().equals(elib.getExcelData("P1 Forms", 295, 4)))
					{
						element.click();
						
					}
					}
				
					
				}
				
			}
 
		}


	/**
	 * Numeric
	 * Verify mandatory option can be unchecked
	 * @throws Throwable
	 */
	@Test
	public void VerifyTC223_Numeric_MarkAsMandatory_Uncheck() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 297, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 297, 2));
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 297, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 297, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 297, 2), fPage.getSelectTableDropdown().getText());
		fPage.getColumnSettingsTab().click();
		List<WebElement> colsList = driver.findElements(By.xpath("//div[@class='forms__columnRestrictions__columnItem___2IY09']"));
		for(WebElement w : colsList)
		{
			String text = w.getText();
				if(text.equals(elib.getExcelData("P1 Forms", 297, 3))) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")));
					driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
					wlib.waitforElement(driver.findElement(By.xpath("//span[.='Mark as mandatory']")));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//span[.='Mark as mandatory']")));
					List<WebElement> list1 = driver.findElements(By.xpath("//span[@class='mdl-checkbox__label']"));
					for(WebElement element : list1)
					{
					if(element.getText().equals(elib.getExcelData("P1 Forms", 297, 4)))
					{
						element.click();
						fPage.getSaveButton().click();
						wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")));
						driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
						wlib.waitforElement(driver.findElement(By.xpath("//span[.='Mark as mandatory']")));
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//span[.='Mark as mandatory']")));
						element.click();
					}
					}

				}

			}
 
		}
}
