package ai.qa.FormsTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC069_Test extends BaseClass{

	/**
	 * Add role & state
	 * Used to verify clicking on + sign adds new combination of Role and State
	 * @throws Throwable
	 */
	@Test
	public void verifyTC069() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 113, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 113, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 113, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 113, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 113, 2), fPage.getSelectTableDropdown().getText());
		int oldRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		int oldStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		try {
		fPage.getAddIcon().click();
		}
		catch(Throwable e)
		{
			
		}
		int newRolesCount = 0;
		int count = 0;
		while(count<10)
		{
			try {
				newRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
				
				if(newRolesCount>oldRolesCount)
				{
					
				}
				break;
			}
			catch(Throwable e) {
				count++;
			}
		}
		int newStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		Assert.assertEquals(oldRolesCount+1, newRolesCount);
		Assert.assertEquals(oldStatessCount+1, newStatessCount);
	}
}
