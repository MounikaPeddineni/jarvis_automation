package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC095_96_97_Business_User_States_Multi_Select_Unselect_Test extends BaseClass{

	/**
	 * Business user
	 * Used to verify Custom execution state can be added for Business user
	 * @throws Throwable
	 */
	@Test
	public void verifyTC095_Business_User_Custom_Execution_State_Select() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 163, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 163, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 163, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 163, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 163, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getSelectRolesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 163, 3));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		//driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectRolesDropdown().click();
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 163, 4));
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		Assert.assertEquals(elib.getExcelData("P1 Forms", 163, 4), driver.findElement(By.xpath("//span[@class='forms__editor__states___2gJlG']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText());
	}
	
	/**
	 * Business user
	 * Used to verify multiple execution state can be added for Business user
	 * @throws Throwable
	 */
	@Test
	public void verifyTC096_Business_User_Multiple_Execution_State_Select() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 165, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 165, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 165, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 165, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 165, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getSelectRolesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 165, 3));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		//driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectRolesDropdown().click();
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 165, 4));
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 165, 5));
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		Assert.assertEquals("2 Options Selected", driver.findElement(By.xpath("//span[@class='forms__editor__states___2gJlG']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText());
	}
	
	/**
	 * Business user
	 * Used to verify execution state can be removed for Business user
	 * @throws Throwable
	 */
	@Test
	public void verifyTC097_Business_User_Execution_State_UnSelect() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 167, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 167, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 167, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 167, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 167, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getSelectRolesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 167, 3));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		//driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectRolesDropdown().click();
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 167, 4));
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 167, 5));
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
		fPage.getSelectStatesDropdown().click();
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 167, 4)+"']")).click();
		fPage.getSelectStatesDropdown().click();
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		Assert.assertEquals(elib.getExcelData("P1 Forms", 167, 5), driver.findElement(By.xpath("//span[@class='forms__editor__states___2gJlG']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText());
	}
}
