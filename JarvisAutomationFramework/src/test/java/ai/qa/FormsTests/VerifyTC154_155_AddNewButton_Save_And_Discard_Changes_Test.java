package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC154_155_AddNewButton_Save_And_Discard_Changes_Test extends BaseClass{

	/**
	 * Add New Button
	 * Used to verify changes are saved when clicked on save button
	 * @throws Throwable
	 */
	@Test
	public void verifyTC154_NewButton_EnterName_SelectTask_SaveChanges() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 247, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 247, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 247, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 247, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 247, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-add___1s2Pi forms__editor__buttonIcon___TikSP']")).click();
		List<WebElement> buttonsList = driver.findElements(By.xpath("//input[@class='mdl-textfield__input']"));
		for(int i=0; i<buttonsList.size(); i++)
		{
			if(i==buttonsList.size()-1)
			{
				driver.findElement(By.xpath("//input[@value='SAVE']/../../../div[4]//input[@class='mdl-textfield__input']")).sendKeys(elib.getExcelData("P1 Forms", 247, 3));
				driver.findElement(By.xpath("//input[@value='"+elib.getExcelData("P1 Forms", 247, 3)+"']//following::span[5]//div[@class='select__indicators css-1wy0on6']")).click();
				driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 247, 4));
				WebElement list1 = driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]"));
				String l[] = list1.getText().split(" ");
				for(int j=0; j<l.length; j++)
				{
					if(l[j].equals(elib.getExcelData("P1 Forms", 247, 4)))
					{
						driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 247, 4)+"']")).click();
					}
				}
				
			}
		}
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		Assert.assertEquals(elib.getExcelData("P1 Forms", 247, 3), driver.findElement(By.xpath("//input[@value='SAVE']/../../../div[4]//input[@class='mdl-textfield__input']")).getAttribute("value"));
		Assert.assertEquals(elib.getExcelData("P1 Forms", 247, 4), driver.findElement(By.xpath("//input[@value='"+elib.getExcelData("P1 Forms", 247, 3)+"']//following::span[5]//div[@class='select__single-value css-1uccc91-singleValue']")).getText());
		
	}
	
	/**
	 * Add New Button
	 * Used to verify changes are discarded when clicked on cancel button
	 * @throws Throwable
	 */
	@Test
	public void verifyTC156_NewButton_EnterName_SelectTask_DiscardChanges() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 249, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 249, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 249, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 249, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 249, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		fPage.getButtonSettingsTab().click();
		driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-add___1s2Pi forms__editor__buttonIcon___TikSP']")).click();
		List<WebElement> oldButtonsList = driver.findElements(By.xpath("//input[@class='mdl-textfield__input']"));
		for(int i=0; i<oldButtonsList.size(); i++)
		{
			if(i==oldButtonsList.size()-1)
			{
				driver.findElement(By.xpath("//input[@value='SAVE']/../../../div[4]//input[@class='mdl-textfield__input']")).sendKeys(elib.getExcelData("P1 Forms", 249, 3));
				driver.findElement(By.xpath("//input[@value='"+elib.getExcelData("P1 Forms", 249, 3)+"']//following::span[5]//div[@class='select__indicators css-1wy0on6']")).click();
				driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 249, 4));
				WebElement list1 = driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]"));
				String l[] = list1.getText().split(" ");
				for(int j=0; j<l.length; j++)
				{
					if(l[j].equals(elib.getExcelData("P1 Forms", 249, 4)))
					{
						driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 249, 4)+"']")).click();
					}
				}
				
			}
		}
		fPage.getCancelButton().click();
		List<WebElement> newButtonsList = driver.findElements(By.xpath("//input[@class='mdl-textfield__input']"));
		Assert.assertEquals(oldButtonsList,newButtonsList);
		
	}
}
