package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC062_63_64_Test extends BaseClass{

	/**
	 * States
	 * Used to verify that state can be unselected
	 * @throws Throwable
	 */
	@Test
	public void verifyTC062() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 101, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 101, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 101, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 101, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 101, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
			}
		int count = 0;
        while(count < 10) {
		   try {
			   List<WebElement> newlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			   if(newlist.size()>oldlist.size());
			   {  
				   newlist.get(newlist.size()-1).click();
			   }
		     break;
		   }catch (Throwable e) {
			count++;
		   }
        }
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 101, 3));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 101, 4));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
		fPage.getSelectStatesDropdown().click();
		String text = driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).getText();
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 101, 3)+"']")).click();
		/*String arr[] = text.split(" ");
		for(int i = 0; i<arr.length; i++)
		{
			if(arr[i].equals(elib.getExcelData("P1 Forms", 101, 3)))
			{
				Thread.sleep(1000);
				driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 101, 3)+"']")).click();
			}
		}*/
		fPage.getSelectStatesDropdown().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 101, 4), driver.findElement(By.xpath("//span[@class='forms__editor__states___2gJlG']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText());
	}
	
	/**
	 * States
	 * Used to verify changes are saved when clicked on save button
	 * @throws Throwable
	 */
	@Test
	public void verifyTC063() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 103, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 103, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 103, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 103, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 103, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
			}
		int count = 0;
        while(count < 10) {
		   try {
			   List<WebElement> newlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			   if(newlist.size()>oldlist.size());
			   {  
				   newlist.get(newlist.size()-1).click();
			   }
		     break;
		   }catch (Throwable e) {
			count++;
		   }
        }
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 103, 3));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 103, 4));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
		fPage.getSelectStatesDropdown().click();
		String text = driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).getText();
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 103, 3)+"']")).click();
		/*String arr[] = text.split(" ");
		for(int i = 0; i<arr.length; i++)
		{
			if(arr[i].equals(elib.getExcelData("P1 Forms", 103, 3)))
			{
				driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 103, 3)+"']")).click();
			}
		}*/
		fPage.getSelectStatesDropdown().click();
		fPage.getSaveButton().click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[.='Your changes have been saved']")).isDisplayed());
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		Thread.sleep(200);
		Assert.assertEquals(elib.getExcelData("P1 Forms", 103, 4), driver.findElement(By.xpath("//span[@class='forms__editor__states___2gJlG']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText());
	}
	
	/**
	 * States
	 * Used to verify changes are discarded when clicked on cancel button
	 * @throws Throwable
	 */
	@Test
	public void verifyTC064() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 105, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 105, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 105, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 105, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 105, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		oldlist.get(oldlist.size()-1).click();
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 105, 3));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 105, 4));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
		fPage.getSelectStatesDropdown().click();
		String text = driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).getText();
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 105, 3)+"']")).click();
		String arr[] = text.split(" ");
		for(int i = 0; i<arr.length; i++)
		{
			if(arr[i].equals(elib.getExcelData("P1 Forms", 105, 3)))
			{
				driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 105, 3)+"']")).click();
			}
		}
		fPage.getSelectStatesDropdown().click();
		String oldText = driver.findElement(By.xpath("//span[@class='forms__editor__states___2gJlG']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText();
		fPage.getCancelButton().click();
		String newText = driver.findElement(By.xpath("//span[@class='forms__editor__states___2gJlG']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText();
		Assert.assertEquals(oldText, newText);
	}
}
