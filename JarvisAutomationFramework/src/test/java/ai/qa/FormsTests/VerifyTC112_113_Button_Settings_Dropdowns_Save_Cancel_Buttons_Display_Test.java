package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC112_113_Button_Settings_Dropdowns_Save_Cancel_Buttons_Display_Test extends BaseClass{

	/**
	 * Main Table
	 * Used for verify 4 dropdowns are displayed for each main table and inline tables.
	 * @throws Throwable
	 */
	@Test
	public void verifyTC112_Four_Dropdowns_Displayed_For_Each_Main_Table() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 197, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 197, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 197, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 197, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 197, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		oldlist.get(0).click();
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 197, 2), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 197, 2)+"']")).getText());
		Assert.assertTrue(driver.findElement(By.xpath("//label[.='Button Name']/preceding::input[@value='CANCEL']")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[1]")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[3]")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[5]")).isDisplayed());
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 197, 3)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 197, 3));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 197, 3)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 197, 3)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 197, 3), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist1 = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		oldlist1.get(0).click();
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist1.get(oldlist1.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist1 = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist1.get(oldlist1.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 197, 3), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 197, 3)+"']")).getText());
		Assert.assertTrue(driver.findElement(By.xpath("//label[.='Button Name']/preceding::input[@value='CANCEL']")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[1]")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[3]")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[5]")).isDisplayed());
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 197, 4)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 197, 4));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 197, 4)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 197, 4)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 197, 4), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist2 = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		oldlist2.get(0).click();
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist2.get(oldlist2.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist2 = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist2.get(oldlist2.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 197, 4), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 197, 4)+"']")).getText());
		Assert.assertTrue(driver.findElement(By.xpath("//label[.='Button Name']/preceding::input[@value='CANCEL']")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[1]")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[3]")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[5]")).isDisplayed());
	
	}
	
	/**
	 * Main Table
	 * Used for verify by default Save and Cancel button settings are displayed for each main table and inline table
	 * @throws Throwable
	 */
	@Test
	public void verifyTC113_Default_Save_Cancel_Buttons_Displayed_For_Each_Main_Table() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 199, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 199, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 199, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 199, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 199, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 199, 2), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 199, 2)+"']")).getText());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='SAVE']")).isDisplayed());
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 199, 3)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 199, 3));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 199, 3)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 199, 3)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 199, 3), fPage.getSelectTableDropdown().getText());
		oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		oldlist.get(0).click();
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 199, 3), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 199, 3)+"']")).getText());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='SAVE']")).isDisplayed());
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 199, 4)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 199,4));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 199,4)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 199,4)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 199,4), fPage.getSelectTableDropdown().getText());
		oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		oldlist.get(0).click();
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 199, 4), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 199, 4)+"']")).getText());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='CANCEL']")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//input[@value='SAVE']")).isDisplayed());
	
	}
}
