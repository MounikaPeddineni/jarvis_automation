package ai.qa.FormsTests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC072_Test extends BaseClass{

	/**
	 * Add role & state
	 * Used to verify changes are discarded when clicked on cancel button
	 * @throws Throwable
	 */
	@Test
	public void verifyTC072() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 117, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 117, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 117, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 117, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 117, 2), fPage.getSelectTableDropdown().getText());
		int oldRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		int oldStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		try {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-close___1_Pix forms__editor__buttonIcon___TikSP']")));
		driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-close___1_Pix forms__editor__buttonIcon___TikSP']")).click();
			fPage.getAddIcon().click();
		}
		catch(Throwable e)
		{
			
		}	
		fPage.getCancelButton().click();
		int newRolesCount = 0;
		int count = 0;
		while(count<10)
		{
			try {
				newRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
				
				if(newRolesCount==oldRolesCount)
				{
			
				}
				break;
			}
			catch(Throwable e) {
				count++;
			}
		}
		int newStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		Assert.assertEquals(oldRolesCount, newRolesCount);
		Assert.assertEquals(oldStatessCount, newStatessCount);
		
	}
}
