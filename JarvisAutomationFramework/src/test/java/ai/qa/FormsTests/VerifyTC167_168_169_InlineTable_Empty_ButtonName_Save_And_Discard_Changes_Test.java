package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC167_168_169_InlineTable_Empty_ButtonName_Save_And_Discard_Changes_Test extends BaseClass {

	/**
	 * Inline Table
	 * Verify button name cannot be set as blank
	 * @throws Throwable
	 */
	@Test
	public void verifyTC167_Button_Name_Cannot_Be_Blank() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 263, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 263, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 263, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 263, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 263, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 263, 2), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 263, 2)+"']")).getText());
		Assert.assertEquals(elib.getExcelData("P1 Forms", 263, 3), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 263, 3)+"']")).getText());
		driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 263, 3)+"']/following::input[@value='CANCEL']")).clear();
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));

	}
	
	/**
	 * Inline Table
	 * Verify changes are saved when clicked on save button
	 * @throws Throwable
	 */
	@Test
	public void verifyTC168_Rename_Buttonname_Save_Changes() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 265, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 265, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 265, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 265, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 265, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 265, 2), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 263, 2)+"']")).getText());
		Assert.assertEquals(elib.getExcelData("P1 Forms", 265, 3), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 263, 3)+"']")).getText());
		driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 265, 3)+"']/following::input[@value='CANCEL']")).clear();
		driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 265, 3)+"']/following::input[@value='CANCEL']")).sendKeys(elib.getExcelData("P1 Forms", 265, 4));
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		Assert.assertEquals(elib.getExcelData("P1 Forms", 265, 4), driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 265, 3)+"']/following::input[@value='"+elib.getExcelData("P1 Forms", 265, 4)+"']")).getAttribute("value"));
	}
	
	/**
	 * Inline Table
	 * Verify changes are discarded when clicked on cancel button
	 */
	/*@Test
	public void verifyTC169_Rename_ButtonName_Discard_Changes() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 267, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 267, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 267, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 267, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 267, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 267, 2), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 263, 2)+"']")).getText());
		Assert.assertEquals(elib.getExcelData("P1 Forms", 267, 3), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 263, 3)+"']")).getText());
		driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 267, 3)+"']/following::input[@value='CANCEL']")).clear();
		driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 267, 3)+"']/following::input[@value='CANCEL']")).sendKeys(elib.getExcelData("P1 Forms", 267, 4));
		fPage.getCancelButton().click();
		//wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		//Assert.assertEquals(elib.getExcelData("P1 Forms", 267, 4), driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 267, 3)+"']/following::input[@value='"+elib.getExcelData("P1 Forms", 267, 4)+"']")).getAttribute("value"));
	}*/
}
