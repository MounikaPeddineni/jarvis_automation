package ai.qa.FormsTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.JavaUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.DataSetsPage;
import ai.qa.Pages.FormsPage;
import ai.qa.Pages.SettingsPage;
import ai.qa.Pages.TaskPage;

public class TenantInitialRun_Test extends BaseClass{

	@Test
	public void TC_1_creatingAPP() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		SettingsPage sPage = new SettingsPage(driver);
		JavaUtility jlib = new JavaUtility();
		ExcelUtility elib = new ExcelUtility();
		
		aPage.getSettingsIcon().click();
		String categoryName = "AppCategory "+jlib.getRanDomData();
		elib.setExcelData("P1 Regression", 1, 0, categoryName);
		sPage.createCategory(categoryName);
		aPage.getAppSummaryIcon().click();
		aPage.createApp(categoryName, elib.getExcelData("P1 Regression", 1, 1) , elib.getExcelData("P1 Regression", 1, 2));
	}
	
	@Test
	public void TC_2_creatingTables() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		DataSetsPage dPage = new DataSetsPage(driver);
		dPage.navigateToDataSets();
		dPage.createJiffyTable(elib.getExcelData("P1 Forms", 5, 2));
		Thread.sleep(500);
		fPage.navigateToForms();
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 5, 2));
		fPage.getAddNewFormButton().click();
		fPage.getSaveButton().click();
		dPage.navigateToDataSets();
		dPage.createJiffyTable(elib.getExcelData("P1 Forms", 19, 2));
		fPage.navigateToForms();
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 19, 2));
		dPage.navigateToDataSets();
		dPage.createJiffyTable(elib.getExcelData("P1 Forms", 31, 2));
		fPage.navigateToForms();
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 31, 2));
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		
	}
	
	@Test
	public void TC_3_creatingTasks() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		TaskPage tPage = new TaskPage(driver);
		JavaUtility jlib = new JavaUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		tPage.navigateToTasks();
		String taskname = "Task"+jlib.getRanDomData();
		elib.setExcelData("P1 Forms", 209, 3, taskname);
		tPage.createTask(elib.getExcelData("P1 Forms", 209, 3));
		tPage.createTask(elib.getExcelData("P1 Forms", 211, 3));
		tPage.createTask(elib.getExcelData("P1 Forms", 213, 3));
	}
	
	@Test
	public void TC_4_creatingInlineTable() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		DataSetsPage dPage = new DataSetsPage(driver);
		dPage.navigateToDataSets();
		dPage.createDatasetWithInlineTable(elib.getExcelData("P1 Forms", 259, 2), elib.getExcelData("P1 Forms", 259, 3), elib.getExcelData("P1 Forms", 259, 4), elib.getExcelData("P1 Forms", 259, 5));
		
	}

	@Test
	public void TC_5_JiffyTableWithColumnsCreation() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		DataSetsPage dPage = new DataSetsPage(driver);
		dPage.navigateToDataSets();
		dPage.getCreateDatasetIcon().click();
		driver.findElement(By.xpath("//span[@data-title='Jiffy Table']")).click();
		dPage.getjiffyTableNameTextField().sendKeys(elib.getExcelData("P1 Forms", 269, 2));
		driver.findElement(By.xpath("//div[@class='data-table__dcb-datatable__table___2Ggwt']//div[2]//div[@class='select__single-value css-1uccc91-singleValue']")).click();
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Column7")).sendKeys(elib.getExcelData("P1 Forms", 269, 3));
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys("Singleline");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Column8")).sendKeys(elib.getExcelData("P1 Forms", 273, 3));
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys("Singleline");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		driver.findElement(By.xpath("//button[.='Create']")).click();	
		
	}
	
	@Test
	public void TC_6_JiffyTableWithALLColumnsCreation() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		DataSetsPage dPage = new DataSetsPage(driver);
		dPage.navigateToDataSets();
		dPage.getCreateDatasetIcon().click();
		driver.findElement(By.xpath("//span[@data-title='Jiffy Table']")).click();
		dPage.getjiffyTableNameTextField().sendKeys(elib.getExcelData("P1 Forms", 285, 2));
		driver.findElement(By.xpath("//div[@class='data-table__dcb-datatable__table___2Ggwt']//div[2]//div[@class='select__single-value css-1uccc91-singleValue']")).click();
		
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Column7")).sendKeys(elib.getExcelData("P1 Forms", 285, 3));
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys("Singleline");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Column8")).sendKeys(elib.getExcelData("P1 Forms", 285,4));
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys("Multiline");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Column9")).sendKeys(elib.getExcelData("P1 Forms", 285, 5));
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys("Numeric");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		driver.findElement(By.id("JDI")).click();
		
		driver.findElement(By.id("textfield-Column10")).sendKeys(elib.getExcelData("P1 Forms", 285, 6));
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys("Image Cell");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Column11")).sendKeys(elib.getExcelData("P1 Forms", 285, 7));
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys("Attachment");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Column12")).sendKeys(elib.getExcelData("P1 Forms", 285, 8));
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys("Url");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Column13")).sendKeys(elib.getExcelData("P1 Forms", 285, 9));
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys("Auto Generated");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Column14")).sendKeys(elib.getExcelData("P1 Forms", 285, 10));
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys("Datetime");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		driver.findElement(By.xpath("//button[.='Create']")).click();	
		
		
	}

}
