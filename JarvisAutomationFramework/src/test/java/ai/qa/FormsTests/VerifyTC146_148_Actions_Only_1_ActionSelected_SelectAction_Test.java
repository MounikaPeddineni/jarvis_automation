package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC146_148_Actions_Only_1_ActionSelected_SelectAction_Test extends BaseClass{

	/**
	 * Actions
	 * Used to erify that only 1 Action can be selected at a time
	 * @throws Throwable
	 */
	@Test
	public void verifyTC146_Only_1_Action_Can_Be_Selected() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 235, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 235, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 235, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 235, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 235, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		driver.findElement(By.xpath("//input[@value='SAVE']//following::span[5]//div[@class='select__indicators css-1wy0on6']")).click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 235, 3));
		WebElement list1 = driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]"));
		String l[] = list1.getText().split(" ");
		for(int i=0; i<l.length; i++)
		{
			if(l[i].equals(elib.getExcelData("P1 Forms", 235, 3)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='EXIT']")).click();
			}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 235, 3), driver.findElement(By.xpath("//input[@value='SAVE']//following::span[5]//div[@class='select__single-value css-1uccc91-singleValue']")).getText());
		driver.findElement(By.xpath("//input[@value='SAVE']//following::span[5]//div[@class='select__indicators css-1wy0on6']")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).isDisplayed());
	
		
	}
	
	/**
	 * Actions
	 * Used to verify if action is not selected, error message is displayed
	 * @throws Throwable
	 */
	@Test
	public void verifyTC148_Select_Action() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 237, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 237, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 237, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 237, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 237, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		driver.findElement(By.xpath("//input[@value='SAVE']//following::span[5]//div[@class='select__indicators css-1wy0on6']")).click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 237, 3));
		WebElement list1 = driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]"));
		String l[] = list1.getText().split(" ");
		for(int i=0; i<l.length; i++)
		{
			if(l[i].equals(elib.getExcelData("P1 Forms", 237, 3)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='EXIT']")).click();
			}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 237, 3), driver.findElement(By.xpath("//input[@value='SAVE']//following::span[5]//div[@class='select__single-value css-1uccc91-singleValue']")).getText());

	}
}
