package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC213_214_MarkAsMandatory_Displayed_For_Predined_NotDisplayed_For_UserDefined_Test extends BaseClass{

	/**
	 * MarkAsMandatory
	 * Verify pre-defined columns dont have Mark as mandatory option(UUID, Created by, Created Date, Updated By, Updated Date, Execution State)
	 * @throws Throwable
	 */
	@Test
	public void VerifyTC213_MarkAsMandatory_Displayed_For_Predined() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 275, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 275, 2));
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 275, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 275, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 275, 2), fPage.getSelectTableDropdown().getText());
		fPage.getColumnSettingsTab().click();
		List<WebElement> colsList = driver.findElements(By.xpath("//div[@class='forms__columnRestrictions__columnItem___2IY09']"));
		for(WebElement w : colsList)
		{
			String text = w.getText();
			if(text.equals(elib.getExcelData("P1 Forms", 275, 3)))
			{
				
			}
			else
			{
				if(text.equals(elib.getExcelData("P1 Forms", 275, 4))) {
				}
				else
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")));
					driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
					try {
						driver.findElement(By.xpath("//div[.='Mark as mandatory']")).isDisplayed();
					}
					catch(Throwable e)
					{
						
					}
					driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
				}
			}
 
		}

	}
	
	/**
	 * Marks as mandatory
	 * Verify mark as mandatory option is available for all other columns (other than pre-defined)
	 * @throws Throwable
	 */
	@Test
	public void VerifyTC214_MarkAsMandatory_NotDisplayed_For_UserDefined() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 277, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 277, 2));
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 277, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 277, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 277, 2), fPage.getSelectTableDropdown().getText());
		fPage.getColumnSettingsTab().click();
		List<WebElement> colsList = driver.findElements(By.xpath("//div[@class='forms__columnRestrictions__columnItem___2IY09']"));
		for(WebElement w : colsList)
		{
			String text = w.getText();
			if(text.equals(elib.getExcelData("P1 Forms", 277, 3)))
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")));
				driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
				try {
					driver.findElement(By.xpath("//div[.='Mark as mandatory']")).isDisplayed();
				}
				catch(Throwable e)
				{
					
				}
				driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
			}
			
			if(text.equals(elib.getExcelData("P1 Forms", 277, 4)))
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")));
				driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
				try {
					driver.findElement(By.xpath("//div[.='Mark as mandatory']")).isDisplayed();
				}
				catch(Throwable e)
				{
					
				}
				driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
			}
				
			}
 
		}

}
