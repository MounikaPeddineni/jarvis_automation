package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC114_119_Button_Settings_Empty_TaskAssociated_ExecutionState_ButtonName_Test extends BaseClass{

	/**
	 * Main Table
	 * Used to verify by default Task Associated and Execution state is not selected
	 * @throws Throwable
	 */
	@Test
	public void verifyTC114_Default_Task_Associated_and_Execution_State_Not_Selected() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 201, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 201, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 201, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 201, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 201, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 201, 2), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 201, 2)+"']")).getText());
		Assert.assertEquals("Select", driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[1]//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']")).getText());
		Assert.assertEquals("Select", driver.findElement(By.xpath("//input[@value='CANCEL']/following::span[3]//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']")).getText());
	}
	
	/**
	 * Main Table
	 * Used to verify button name cannot be set as blank
	 * @throws Throwable
	 */
	@Test
	public void verifyTC119_Button_Name_Cannot_Be_Blank() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 203, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 203, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 203, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 203, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 203, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 203, 2), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 203, 2)+"']")).getText());
		driver.findElement(By.xpath("//input[@value='CANCEL']")).clear();
		fPage.getSaveButton().click();
		wlib.waitForElementtInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
	}
}
