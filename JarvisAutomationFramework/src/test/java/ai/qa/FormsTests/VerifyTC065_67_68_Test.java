package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC065_67_68_Test extends BaseClass{

	@Test
	public void verifyTC065() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 107, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 107, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 107, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 107, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 107, 2), fPage.getSelectTableDropdown().getText());
		int newRolesCount = driver.findElements(By.xpath("//span[@class='forms__editor__roles___2SNoy']")).size();
		int newStatessCount = driver.findElements(By.xpath("//span[@class='forms__editor__states___2gJlG']")).size();
		Assert.assertTrue(newRolesCount>=1);
		Assert.assertTrue(newStatessCount>=1);
	}
	
	@Test
	public void verifyTC067() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 109, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 109, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 109, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 109, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 109, 2), fPage.getSelectTableDropdown().getText());
		fPage.getSelectRolesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 109, 3));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectRolesDropdown().click();
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 109, 4));
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
		String oldformrole = driver.findElement(By.xpath("//span[@class='forms__editor__roles___2SNoy']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText();
		String oldformstate = driver.findElement(By.xpath("//span[@class='forms__editor__states___2gJlG']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText();
		fPage.getDuplicateFormIcon().click();
		List<WebElement> newlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		newlist.get(1).click();
		String newformrole = driver.findElement(By.xpath("//span[@class='forms__editor__roles___2SNoy']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText();
		String newformstate = driver.findElement(By.xpath("//span[@class='forms__editor__states___2gJlG']//div[@class='select__value-container select__value-container--is-multi select__value-container--has-value css-1hwfws3']")).getText();
		Assert.assertEquals(oldformrole, newformrole);
		Assert.assertEquals(oldformstate, newformstate);
	}
	
}
