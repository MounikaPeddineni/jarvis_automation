package ai.qa.FormsTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC001_02_Test extends BaseClass{
	/**
	 * Used to verify that form settings page loads successfully with 3 different tabs
	 * @throws Throwable
	 */
	@Test
	public void verifyTC001() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		Assert.assertTrue(driver.findElement(By.xpath("//span[.='Forms']")).isDisplayed());
		Assert.assertTrue(fPage.getGeneralSettingsTab().isDisplayed());
		Assert.assertTrue(fPage.getButtonSettingsTab().isDisplayed());
		Assert.assertTrue(fPage.getColumnSettingsTab().isDisplayed());	

	}
	

	/**
	 * Used to verify that table dropdown is displayed in form settings screen
	 * @throws Throwable
	 */
	@Test
	public void verifyTC002() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();		
		Assert.assertTrue(fPage.getSelectTableDropdown().isDisplayed());
	}
}
