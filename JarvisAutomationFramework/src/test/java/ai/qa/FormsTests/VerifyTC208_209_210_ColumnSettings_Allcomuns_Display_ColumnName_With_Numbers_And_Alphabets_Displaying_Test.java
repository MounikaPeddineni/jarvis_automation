package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.DataSetsPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC208_209_210_ColumnSettings_Allcomuns_Display_ColumnName_With_Numbers_And_Alphabets_Displaying_Test extends BaseClass{

	/**
	 * ColumnSettings
	 * Verify all the columns are displayed
	 * @throws Throwable
	 */
	@Test
	public void VerifyTC208_AllColumns_Displaying_Properly() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 269, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 269, 2));
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 269, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 269, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 269, 2), fPage.getSelectTableDropdown().getText());
		DataSetsPage dPage = new DataSetsPage(driver);
		dPage.navigateToDataSets();
		driver.findElement(By.xpath("//a[.='"+elib.getExcelData("P1 Forms", 269, 2)+"']")).click();
		List<WebElement> dataSetColList = driver.findElements(By.xpath("//div[@class='fixedDataTableCellLayout_main public_fixedDataTableCell_main']"));
		fPage.navigateToForms();
		fPage.getColumnSettingsTab().click();
		List<WebElement> formsCollist = driver.findElements(By.xpath("//div[@class='forms__columnRestrictions__columnName___1lGnd']"));
		Assert.assertEquals(dataSetColList.size()-1, formsCollist.size());
		
	}
	
	/**
	 * Column Settings
	 * Verify that column with numbers is displayed properly
	 * @throws Throwable
	 */
	@Test
	public void VerifyTC209_ColumnName_with_Numbers_Displaying_Properly() throws Throwable {
		
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 271, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 271, 2));
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 271, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 271, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 271, 2), fPage.getSelectTableDropdown().getText());
		fPage.getColumnSettingsTab().click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 271, 3)+"']")).isDisplayed());
		
		
	}

	/**
	 * Column Settings
	 * Verify that column with alphabets is displayed properly
	 * @throws Throwable
	 */
	@Test
	public void VerifyTC210_ColumnName_with_Alphabets_Displaying_Properly() throws Throwable 
	{
	
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 273, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 273, 2));
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 273, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 273, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 273, 2), fPage.getSelectTableDropdown().getText());
		fPage.getColumnSettingsTab().click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 273, 3)+"']")).isDisplayed());
		
	
	}
}
