package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.DataSetsPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC003_04_Test extends BaseClass{
	/**
	 * Used to verify that table can be selected from the Table dropdown
	 * @throws Throwable
	 */
	@Test
	public void verifyTC003() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 5, 2)))
		{
		
		}
		else
		{
			fPage.getSelectTableDropdown().click();
			driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 5, 2));
			//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
			WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
			String list[] = element.getText().split(" ");
			for(int i=0; i<list.length;i++)
			{
				if(list[i].equals(elib.getExcelData("P1 Forms", 5, 2)))
				{
					driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 5, 2)+"']")).click();
				}
			}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 5, 2), fPage.getSelectTableDropdown().getText());
		
	}
	
	/**
	 * Used to verify that all jiffy tables are displayed in the dropdown
	 * @throws Throwable
	 */
	@Test
	public void verifyTC004() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		fPage.getSelectTableDropdown().click();
		String tlist[] = null;
		try {
			WebElement tableList = driver.findElement(By.xpath("//div[@class='select__menu css-tjxlve-menu']"));			
			tlist = tableList.getText().split("\n");
		}
		catch(Throwable e)
		{
			e.printStackTrace();
		}
		DataSetsPage dataPage = new DataSetsPage(driver);
		dataPage.navigateToDataSets();
		List<WebElement> dataSetList = driver.findElements(By.xpath("//div[.='Jiffy Table' or .='Document Table']/div/div//preceding::a[1]"));
		Assert.assertEquals(tlist.length,dataSetList.size());
	}
}
















