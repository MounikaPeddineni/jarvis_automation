package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;
import ai.qa.Pages.TaskPage;

public class VerifyTC126_128_TaskAssociated_All_Tasks_Displaying_1_Task_CanBe_Selected_Test extends BaseClass{

	/**
	 * Task Associated dropdown
	 * Used to verify all the tasks are displayed in the dropdown
	 * @throws Throwable
	 */
	@Test
	public void verifyTC126_All_Tasks_Displaying() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 215, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 215, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 215, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 215, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 215, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		driver.findElement(By.xpath("//input[@value='CANCEL']//following::span[1]//div[@class='select__indicators css-1wy0on6']")).click();
		Thread.sleep(1000);
		wlib.waitforElement(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 215, 3)+"']")));
		WebElement tasks = driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]"));
		String taskList[] = tasks.getText().split("\n");
		TaskPage tPage = new TaskPage(driver);
		List<WebElement> t = tPage.getAllTasks();
		Assert.assertEquals(taskList.length, t.size());

	}
	
	/**
	 * Task Associated dropdown
	 * Used to verify that only 1 task can be selected at a time
	 * @throws Throwable
	 */
	@Test
	public void verifyTC128_Only_1_Task_Can_Be_Selected() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		WebDriverUtility wlib = new WebDriverUtility();
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 217, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 217, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 217, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 217, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 217, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		driver.findElement(By.xpath("//input[@value='CANCEL']//following::span[1]//div[@class='select__indicators css-1wy0on6']")).click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 217, 3));
		wlib.waitForElementtVisibality(driver, driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 217, 3)+"']")));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 217, 3), driver.findElement(By.xpath("//div[@class='select__value-container select__value-container--has-value css-1hwfws3']/div[.='"+elib.getExcelData("P1 Forms", 217, 3)+"']")).getText());
		driver.findElement(By.xpath("//input[@value='CANCEL']//following::span[1]//div[@class='select__indicators css-1wy0on6']")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).isDisplayed());
	
		
	}
}
