package ai.qa.FormsTests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppPage;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;
import ai.qa.Pages.PresentationsPage;

public class VerifyTC041_42_Test extends BaseClass{

	/**
	 * Roles
	 * Used to verify for default form, all roles are displayed (including custom role)
	 * @throws Throwable
	 */
	@Test
	public void verifyTC041() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		fPage.getSelectTableDropdown().click();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 71, 2)))
		{
		
		}
		else
		{
			fPage.getSelectTableDropdown().click();
			driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 71, 2));
			//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
			WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
			String list[] = element.getText().split(" ");
			for(int i=0; i<list.length;i++)
			{
				if(list[i].equals(elib.getExcelData("P1 Forms", 71, 2)))
				{
					driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 71, 2)+"']")).click();
				}
			}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 71, 2), fPage.getSelectTableDropdown().getText());

		fPage.getSelectRolesDropdown().click(); 
		//WebElement roles = driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]"));
		WebElement roles = driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]"));
		String rolesList[] = roles.getText().split("\n");
		/*ArrayList<String> newList = new ArrayList<String>();
		for(int i=0; i<rolesList.length; i++)
		{
			newList.add(rolesList[i]);
		}*/
		String list2[] = {"DESIGNER", "SUPPORT", "BUSINESS_USER", "RELEASE_ADMIN"};
		/*List<String> nList = Arrays.asList(list);  
		Assert.assertEquals(nList, newList);*/
		Assert.assertEquals(rolesList, list2);
	}
	
	/**
	 * Roles
	 * Used to verify for default form, all roles are selected (including custom role)
	 * @throws Throwable
	 */
	@Test
	public void verifyTC042() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		fPage.getSelectTableDropdown().click();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 73, 2)))
		{
		
		}
		else
		{
			fPage.getSelectTableDropdown().click();
			driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 73, 2));
			//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
			WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
			String list[] = element.getText().split(" ");
			for(int i=0; i<list.length;i++)
			{
				if(list[i].equals(elib.getExcelData("P1 Forms", 73, 2)))
				{
					driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 73, 2)+"']")).click();
				}
			}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 73, 2), fPage.getSelectTableDropdown().getText());
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		if(driver.findElement(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span")).getText().equals(elib.getExcelData("P1 Forms", 73, 3)))
		{
			Assert.assertTrue(driver.findElement(By.xpath("//div[.='4 options selected']")).isDisplayed());
		}
	}
}
