package ai.qa.FormsTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;

public class VerifyTC160_161_InlineTable_Dropdowns_Save_Cancel_Buttons_Display_Test extends BaseClass {
	
	/**
	 * Inline Table
	 * Verify 4 dropdowns are displayed for each main table and inline tables.
	 * @throws Throwable
	 */
	@Test
	public void verifyTC160_Four_Dropdowns_Displayed_For_Inline_Table() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 259, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 259, 2));
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 259, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 259, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 259, 2), fPage.getSelectTableDropdown().getText());
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 259, 2), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 259, 2)+"']")).getText());
		Assert.assertEquals(elib.getExcelData("P1 Forms", 259, 3), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 259, 3)+"']")).getText());
		Assert.assertTrue(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 259, 3)+"']/following::input[@value='CANCEL']")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 259, 3)+"']/following::input[@value='CANCEL']/following::span[1]")).isDisplayed());
		//Assert.assertTrue(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 259, 3)+"']/following::input[@value='CANCEL']/following::span[3]")).isDisplayed());
		//Assert.assertTrue(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 259, 3)+"']/following::input[@value='CANCEL']/following::span[5]")).isDisplayed());
		
	}

	/**
	 * Inline Table
	 * Verify by default Save and Cancel button settings are displayed for each main table and inline table
	 * @throws Throwable
	 */
	@Test
	public void verifyTC161_Default_Save_Cancel_Buttons_Displayed_For_Inline_Table() throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		FormsPage fPage = new FormsPage(driver);
		
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		fPage.navigateToForms();
		if(fPage.getSelectTableDropdown().getText().equals(elib.getExcelData("P1 Forms", 261, 2)))
		{
		
		}
		else
		{
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 261, 2));
		//driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP forms__selectTable___2BcgY css-2b097c-container']/following::div[2]")).click();
		//driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']")).click();
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(elib.getExcelData("P1 Forms", 261, 2)))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+elib.getExcelData("P1 Forms", 261, 2)+"']")).click();
			}
		}
		}
		Assert.assertEquals(elib.getExcelData("P1 Forms", 261, 2), fPage.getSelectTableDropdown().getText());
		List<WebElement> oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
		try {
			fPage.getAddNewFormButton().click();
			oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
			oldlist.get(oldlist.size()-1).click();
			}
			catch(Throwable e)
			{
				oldlist.get(oldlist.size()-1).click();
				fPage.getDeleteFormIcon().click();
				driver.findElement(By.xpath("//button[.='YES']")).click();
				fPage.getAddNewFormButton().click();
				oldlist = driver.findElements(By.xpath("//div[@class='forms__form-item__accordHead___3X5oJ undefined']/span"));
				oldlist.get(oldlist.size()-1).click();
			}
		fPage.getButtonSettingsTab().click();
		Assert.assertEquals(elib.getExcelData("P1 Forms", 261, 2), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 261, 2)+"']")).getText());
		Assert.assertEquals(elib.getExcelData("P1 Forms", 261, 3), driver.findElement(By.xpath("//div[@class='forms__editor__heading___-gx9c']/div[.='"+elib.getExcelData("P1 Forms", 261, 3)+"']")).getText());
		Assert.assertTrue(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 261, 3)+"']/following::input[@value='CANCEL']")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//div[.='"+elib.getExcelData("P1 Forms", 261, 3)+"']/following::input[@value='SAVE']")).isDisplayed());

	}
}
