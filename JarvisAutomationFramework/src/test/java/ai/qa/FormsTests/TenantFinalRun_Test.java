package ai.qa.FormsTests;

import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.SettingsPage;

public class TenantFinalRun_Test extends BaseClass{

	@Test
	public void deleteCategory() throws Throwable
	{
		SettingsPage sPage = new SettingsPage(driver);
		ExcelUtility elib = new ExcelUtility();
		sPage.deleteCategory(elib.getExcelData("P1 Regression", 1, 0));	
	}
}
