package ai.qa.RegressionValidations;

import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.ExecutionsPage;

public class AllEmailNodesWebFileIdValidation_Test extends BaseClass {
	
	@Test
	public void checkAllEmailNodesWebFileId() throws Throwable {
	
		ExcelUtility elib = new ExcelUtility();
		ExecutionsPage ePage = new ExecutionsPage(driver);
		ePage.checkStatus(elib.getExcelData("P1 Regression", 13, 2));
		
	}	

	
}
