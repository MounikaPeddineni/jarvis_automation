package ai.qa.RegressionValidations;

import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.ExecutionsPage;

public class GraphProtocolAllEmailNodesServerValidation_Test extends BaseClass{

	@Test
	public void checkGraphProtocolAllEmailNodesServer() throws Throwable {
	
		ExecutionsPage ePage = new ExecutionsPage(driver);
		ExcelUtility elib = new ExcelUtility();
		ePage.checkStatus(elib.getExcelData("P1 Regression", 40, 2));
		
	}	
}
