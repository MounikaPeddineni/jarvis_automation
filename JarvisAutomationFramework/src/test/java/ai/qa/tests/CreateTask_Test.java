package ai.qa.tests;

import org.testng.annotations.Test;
import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.JavaUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.TaskPage;

public class CreateTask_Test extends BaseClass{

	JavaUtility jlib = new JavaUtility();
	@Test
	public void CreateTask() throws Throwable
	{
		AppSummaryPage apage = new AppSummaryPage(driver);
		apage.openApp("123","Automation_Test");
		TaskPage task = new TaskPage(driver);
		task.createTask("DemoTask_"+jlib.getRanDomData());
	}
}
