package ai.qa.tests;

import org.testng.annotations.Test;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.ExecutionsPage;

public class StoredProcedureClusterMode_Test extends BaseClass {

	@Test
	public void runStoredProcedureClusterMode() throws Throwable {
	
		AppSummaryPage aPage = new AppSummaryPage(driver);
		ExcelUtility elib = new ExcelUtility();
		ExecutionsPage ePage = new ExecutionsPage(driver);
		aPage.openApp(elib.getExcelData("P1 Regression", 1, 0),elib.getExcelData("P1 Regression", 1, 1));
		ePage.runTaskInExecutionBot(elib.getExcelData("P1 Regression", 70, 2));
		
		
	}	
}
