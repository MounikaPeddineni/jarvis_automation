package ai.qa.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class VerifyPrivacyPolicy_Test {

	@Test
	public void verifyPrivacyPolicy() throws Throwable {
	
		WebDriver driver = new ChromeDriver();
	
		driver.get("https://tenant.jiffy4-1qa.jiffy.ai/");
		driver.findElement(By.linkText("Privacy Policy")).click();
		for (String handle: driver.getWindowHandles()) {
			   driver.switchTo().window(handle);
		}
		Assert.assertEquals("Your privacy is important to us - jiffy.ai", driver.getTitle());
		
		
	}
}
